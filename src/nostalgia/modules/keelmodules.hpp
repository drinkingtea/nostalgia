/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

namespace nostalgia {

void registerKeelModules() noexcept;

}
