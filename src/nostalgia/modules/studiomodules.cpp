/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <keel/module.hpp>

#include <studioapp/studioapp.hpp>

#include <nostalgia/core/studiomodule.hpp>
#include <nostalgia/scene/studiomodule.hpp>

namespace nostalgia {

static bool modulesRegistered = false;
void registerStudioModules() noexcept {
	if (modulesRegistered) {
		return;
	}
	modulesRegistered = true;
	studio::registerModule(core::studioModule());
	//studio::registerModule(scene::studioModule());
}

}
