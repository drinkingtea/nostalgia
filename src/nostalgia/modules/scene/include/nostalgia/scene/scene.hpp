/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <nostalgia/core/context.hpp>

#include "scenestatic.hpp"

namespace nostalgia::scene {

class Scene {
	private:
		SceneStatic const&m_sceneStatic;

	public:
		explicit Scene(SceneStatic const&sceneStatic) noexcept;

		ox::Error setupDisplay(core::Context &ctx) const noexcept;

	private:
		void setupLayer(core::Context&, ox::Vector<uint16_t> const&layer, unsigned layerNo) const noexcept;

};

}
