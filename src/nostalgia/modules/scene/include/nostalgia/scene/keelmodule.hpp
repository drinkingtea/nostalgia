/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <keel/module.hpp>

namespace nostalgia::scene {

const keel::Module *keelModule() noexcept;

}
