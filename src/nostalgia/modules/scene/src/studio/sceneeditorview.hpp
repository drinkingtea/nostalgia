/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <glutils/glutils.hpp>

#include <nostalgia/core/context.hpp>
#include <nostalgia/core/gfx.hpp>
#include <nostalgia/scene/scene.hpp>

namespace nostalgia::scene {

class SceneEditorView {

	private:
		core::ContextUPtr m_cctx;
		SceneStatic const&m_sceneStatic;
		Scene m_scene;
		glutils::FrameBuffer m_frameBuffer;
		int m_scale = 1;
		ox::Size m_scaleSz = core::gl::drawSize(m_scale);

	public:
		SceneEditorView(turbine::Context &ctx, SceneStatic const&sceneStatic);

		ox::Error setupScene() noexcept;

		void draw(ox::Size const&targetSz) noexcept;

		[[nodiscard]]
		glutils::FrameBuffer const&framebuffer() const noexcept;

};

}
