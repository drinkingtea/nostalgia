/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <studio/studio.hpp>

#include "sceneeditor-imgui.hpp"

namespace nostalgia::scene {

constexpr ox::StringLiteral FileExt_nscn("nscn");

class StudioModule: public studio::Module {
	public:
		ox::Vector<studio::EditorMaker> editors(turbine::Context &ctx) const noexcept override {
			return {
				 studio::editorMaker<SceneEditorImGui>(ctx, FileExt_nscn),
			};
		}
		ox::Vector<ox::UPtr<studio::ItemMaker>> itemMakers(turbine::Context&) const noexcept override {
			ox::Vector<ox::UPtr<studio::ItemMaker>> out;
			out.emplace_back(ox::make<studio::ItemMakerT<SceneDoc>>("Scene", "Scenes", FileExt_nscn));
			return out;
		}
};

static StudioModule const mod;
const studio::Module *studioModule() noexcept {
	return &mod;
}

}
