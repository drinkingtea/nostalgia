/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <turbine/context.hpp>

#include <nostalgia/scene/scene.hpp>

namespace nostalgia::scene {

class SceneEditor {

	private:
		turbine::Context &m_ctx;
		ox::String m_itemName;
		ox::String m_itemPath;
		SceneStatic m_scene;

	public:
		SceneEditor(turbine::Context &ctx, ox::CRStringView path);

		[[nodiscard]]
		SceneStatic const&scene() const noexcept {
			return m_scene;
		}

	protected:
		ox::Error saveItem() noexcept;

};

}
