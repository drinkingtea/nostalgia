/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <keel/keel.hpp>

#include "sceneeditor.hpp"

namespace nostalgia::scene {

SceneEditor::SceneEditor(turbine::Context &ctx, ox::CRStringView path):
	m_ctx(ctx),
	m_scene(*keel::readObj<SceneStatic>(keelCtx(m_ctx), path).unwrapThrow()) {
}

}
