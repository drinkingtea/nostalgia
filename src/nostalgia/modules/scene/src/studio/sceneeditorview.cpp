/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <nostalgia/core/gfx.hpp>

#include "sceneeditorview.hpp"

namespace nostalgia::scene {

SceneEditorView::SceneEditorView(turbine::Context &tctx, SceneStatic const&sceneStatic):
	m_cctx(core::init(tctx, {.glInstallDrawer = false}).unwrapThrow()),
	m_sceneStatic(sceneStatic),
	m_scene(m_sceneStatic) {
}

ox::Error SceneEditorView::setupScene() noexcept {
	glutils::resizeInitFrameBuffer(m_frameBuffer, core::gl::drawSize(m_scale));
	return m_scene.setupDisplay(*m_cctx);
}

void SceneEditorView::draw(ox::Size const&targetSz) noexcept {
	auto const scaleSz = targetSz / core::gl::drawSize(1);
	if (m_scaleSz != scaleSz) [[unlikely]] {
		m_scale = ox::max(1, ox::max(scaleSz.width, scaleSz.height));
		glutils::resizeInitFrameBuffer(m_frameBuffer, core::gl::drawSize(m_scale));
	}
	glutils::FrameBufferBind const frameBufferBind(m_frameBuffer);
	core::gl::draw(*m_cctx, m_scale);
}

glutils::FrameBuffer const&SceneEditorView::framebuffer() const noexcept {
	return m_frameBuffer;
}

}
