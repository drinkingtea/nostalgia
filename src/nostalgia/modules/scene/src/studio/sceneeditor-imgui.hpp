/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <studio/studio.hpp>

#include <turbine/context.hpp>

#include "sceneeditor.hpp"
#include "sceneeditorview.hpp"

namespace nostalgia::scene {

class SceneEditorImGui: public studio::Editor {

	private:
		turbine::Context &m_ctx;
		SceneEditor m_editor;
		SceneEditorView m_view;

	public:
		SceneEditorImGui(turbine::Context &ctx, ox::StringView path);

		void draw(turbine::Context&) noexcept final;

		void onActivated() noexcept override;

	protected:
		ox::Error saveItem() noexcept final;

};

}
