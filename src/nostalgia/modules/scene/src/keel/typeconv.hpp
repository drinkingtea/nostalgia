/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <keel/typeconv.hpp>

#include <nostalgia/scene/scenestatic.hpp>

namespace nostalgia::scene {

class SceneDocToSceneStaticConverter: public keel::Converter<SceneDoc, SceneStatic> {
	ox::Error convert(keel::Context&, SceneDoc &src, SceneStatic &dst) const noexcept final;
};

}
