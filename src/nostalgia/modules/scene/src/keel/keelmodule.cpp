/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <keel/module.hpp>

#include <nostalgia/scene/scenestatic.hpp>

#include "typeconv.hpp"

namespace nostalgia::scene {

class SceneModule: public keel::Module {
	private:
		SceneDocToSceneStaticConverter m_sceneDocToSceneStaticConverter;

	public:
		[[nodiscard]]
		ox::String id() const noexcept override {
			return ox::String("net.drinkingtea.nostalgia.scene");
		}

		[[nodiscard]]
		ox::Vector<keel::TypeDescGenerator> types() const noexcept override {
			return {
				keel::generateTypeDesc<SceneDoc>,
				keel::generateTypeDesc<SceneStatic>,
			};
		}

		[[nodiscard]]
		ox::Vector<const keel::BaseConverter*> converters() const noexcept override {
			return {
				&m_sceneDocToSceneStaticConverter,
			};
		}

		[[nodiscard]]
		ox::Vector<keel::PackTransform> packTransforms() const noexcept override {
			return {
				keel::transformRule<SceneDoc, SceneStatic>,
			};
		}

};

static const SceneModule mod;
const keel::Module *keelModule() noexcept {
	return &mod;
}

}
