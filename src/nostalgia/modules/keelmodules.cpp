/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <nostalgia/core/keelmodule.hpp>
#include <nostalgia/scene/keelmodule.hpp>

namespace nostalgia {

static bool modulesRegistered = false;
void registerKeelModules() noexcept {
	if (modulesRegistered) {
		return;
	}
	modulesRegistered = true;
	keel::registerModule(core::keelModule());
	keel::registerModule(scene::keelModule());
}

}
