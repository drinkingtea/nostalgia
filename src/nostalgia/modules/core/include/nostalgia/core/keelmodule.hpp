/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <keel/module.hpp>

namespace nostalgia::core {

const keel::Module *keelModule() noexcept;

}
