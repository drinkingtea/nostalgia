/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

namespace nostalgia::core {

struct InitParams {
	bool glInstallDrawer = true;
};

}