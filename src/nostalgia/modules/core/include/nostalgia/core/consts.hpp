/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <ox/std/stringliteral.hpp>

namespace nostalgia::core {

constexpr auto TileWidth = 8;
constexpr auto TileHeight = 8;
constexpr auto PixelsPerTile = TileWidth * TileHeight;

constexpr ox::StringLiteral FileExt_ng("ng");
constexpr ox::StringLiteral FileExt_npal("npal");

}
