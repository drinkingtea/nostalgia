/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <ox/fs/fs.hpp>
#include <ox/model/desctypes.hpp>
#include <ox/std/buffer.hpp>
#include <ox/std/size.hpp>

#include <turbine/context.hpp>

#include "initparams.hpp"

namespace nostalgia::core {

class Context;

struct ContextDeleter {
	void operator()(Context *p) noexcept;
};

using ContextUPtr = ox::UPtr<Context, ContextDeleter>;

ox::Result<ContextUPtr> init(turbine::Context &tctx, InitParams const&params = {}) noexcept;

}

