/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <studio/studio.hpp>

namespace nostalgia::core {

const studio::Module *studioModule() noexcept;

}
