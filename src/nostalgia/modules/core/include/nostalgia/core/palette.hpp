/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <ox/std/array.hpp>
#include <ox/std/point.hpp>
#include <ox/std/size.hpp>
#include <ox/std/types.hpp>
#include <ox/std/vector.hpp>
#include <ox/model/def.hpp>


#include "color.hpp"

namespace nostalgia::core {

struct NostalgiaPalette {
	static constexpr auto TypeName = "net.drinkingtea.nostalgia.core.NostalgiaPalette";
	static constexpr auto TypeVersion = 1;
	ox::Vector<Color16> colors = {};
};

struct Palette {
	static constexpr auto TypeName = "net.drinkingtea.nostalgia.core.Palette";
	static constexpr auto TypeVersion = 1;
	ox::Vector<Color16> colors = {};
	[[nodiscard]]
	constexpr Color16 color(auto idx) const noexcept {
		if (idx < colors.size()) [[likely]] {
			return colors[idx];
		}
		return 0;
	}
};

oxModelBegin(NostalgiaPalette)
	oxModelField(colors)
oxModelEnd()

oxModelBegin(Palette)
	oxModelField(colors)
oxModelEnd()

}
