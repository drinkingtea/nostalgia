/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <ox/std/point.hpp>

#include "consts.hpp"

namespace nostalgia::core {

[[nodiscard]]
constexpr std::size_t ptToIdx(int x, int y, int c, int scale = 1) noexcept {
	const auto tileWidth = TileWidth * scale;
	const auto tileHeight = TileHeight * scale;
	const auto pixelsPerTile = tileWidth * tileHeight;
	const auto colLength = static_cast<std::size_t>(pixelsPerTile);
	const auto rowLength = static_cast<std::size_t>(static_cast<std::size_t>(c / tileWidth) * colLength);
	const auto colStart = static_cast<std::size_t>(colLength * static_cast<std::size_t>(x / tileWidth));
	const auto rowStart = static_cast<std::size_t>(rowLength * static_cast<std::size_t>(y / tileHeight));
	const auto colOffset = static_cast<std::size_t>(x % tileWidth);
	const auto rowOffset = static_cast<std::size_t>((y % tileHeight) * tileHeight);
	return static_cast<std::size_t>(colStart + colOffset + rowStart + rowOffset);
}

[[nodiscard]]
constexpr std::size_t ptToIdx(const ox::Point &pt, int c, int scale = 1) noexcept {
	return ptToIdx(pt.x, pt.y, c * TileWidth, scale);
}

[[nodiscard]]
constexpr ox::Point idxToPt(int i, int c, int scale = 1) noexcept {
	const auto tileWidth = TileWidth * scale;
	const auto tileHeight = TileHeight * scale;
	const auto pixelsPerTile = tileWidth * tileHeight;
	// prevent divide by zeros
	if (!c) {
		++c;
	}
	const auto t = i / pixelsPerTile; // tile number
	const auto iti = i % pixelsPerTile; // in tile index
	const auto tc = t % c; // tile column
	const auto tr = t / c; // tile row
	const auto itx = iti % tileWidth; // in tile x
	const auto ity = iti / tileHeight; // in tile y
	return {
		itx + tc * tileWidth,
		ity + tr * tileHeight,
	};
}

static_assert(idxToPt(4, 1) == ox::Point{4, 0});
static_assert(idxToPt(8, 1) == ox::Point{0, 1});
static_assert(idxToPt(8, 2) == ox::Point{0, 1});
static_assert(idxToPt(64, 2) == ox::Point{8, 0});
static_assert(idxToPt(128, 2) == ox::Point{0, 8});
static_assert(idxToPt(129, 2) == ox::Point{1, 8});
static_assert(idxToPt(192, 2) == ox::Point{8, 8});
static_assert(idxToPt(384, 8) == ox::Point{48, 0});

}
