/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <ox/std/types.hpp>

#include <glutils/glutils.hpp>

#include <nostalgia/core/gfx.hpp>
#include <nostalgia/core/context.hpp>

#include "gfx.hpp"

namespace nostalgia::core {

class Context {

	public:
		turbine::Context &turbineCtx;
		glutils::GLProgram bgShader;
		glutils::GLProgram spriteShader;
		ox::Array<renderer::CBB, 4> cbbs;
		renderer::SpriteBlockset spriteBlocks;
		ox::Array<Sprite, 128> spriteStates;
		ox::Array<renderer::Background, 4> backgrounds;
		renderer::Drawer drawer;
		explicit Context(turbine::Context &tctx) noexcept;
		~Context() noexcept;
};

}
