/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <nostalgia/core/context.hpp>

namespace nostalgia::core {
ox::Error initGfx(Context &ctx, InitParams const&) noexcept;
}
