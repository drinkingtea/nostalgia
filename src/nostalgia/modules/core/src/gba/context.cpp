/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <nostalgia/core/gfx.hpp>

#include "context.hpp"

namespace nostalgia::core {

void ContextDeleter::operator()(Context *p) noexcept {
	ox::safeDelete(p);
}

Context::Context(turbine::Context &tctx) noexcept: turbineCtx(tctx) {
}

ox::Error initGfx(Context &ctx, InitParams const&) noexcept;

ox::Result<ContextUPtr> init(turbine::Context &tctx, InitParams const&params) noexcept {
	auto ctx = ox::make_unique<Context>(tctx);
	oxReturnError(initGfx(*ctx, params));
	return ContextUPtr(std::move(ctx));
}

}