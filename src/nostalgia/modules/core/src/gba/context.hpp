/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <nostalgia/core/context.hpp>

namespace nostalgia::core {

class Context {

	public:
		turbine::Context &turbineCtx;

		explicit Context(turbine::Context &tctx) noexcept;
		Context(Context &other) noexcept = delete;
		Context(Context const&other) noexcept = delete;
		Context(Context const&&other) noexcept = delete;
		virtual ~Context() noexcept = default;

		[[nodiscard]]
		ox::MemFS const&rom() const noexcept {
			return static_cast<ox::MemFS const&>(*turbine::rom(turbineCtx));
		}

};

}