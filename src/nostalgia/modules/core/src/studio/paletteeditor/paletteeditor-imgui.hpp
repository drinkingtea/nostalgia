/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <studio/studio.hpp>

#include <nostalgia/core/gfx.hpp>
#include <nostalgia/core/palette.hpp>

namespace nostalgia::core {

class PaletteEditorImGui: public studio::Editor {

	private:
		turbine::Context &m_ctx;
		Palette m_pal;
		std::size_t m_selectedRow = 0;

	public:
		PaletteEditorImGui(turbine::Context &ctx, ox::CRStringView path);

		void draw(turbine::Context&) noexcept final;

	protected:
		ox::Error saveItem() noexcept final;

};

}
