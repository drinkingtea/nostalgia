/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include "palettechangecommand.hpp"

namespace nostalgia::core {

core::PaletteChangeCommand::PaletteChangeCommand(
		TileSheet::SubSheetIdx idx,
		TileSheet &img,
		ox::CRStringView newPalette) noexcept:
		m_img(img),
		m_idx(std::move(idx)),
		m_oldPalette(m_img.defaultPalette),
		m_newPalette(ox::FileAddress(ox::sfmt<ox::BString<43>>("uuid://{}", newPalette))) {
}

void PaletteChangeCommand::redo() noexcept {
	m_img.defaultPalette = m_newPalette;
}

void PaletteChangeCommand::undo() noexcept {
	m_img.defaultPalette = m_oldPalette;
}

int PaletteChangeCommand::commandId() const noexcept {
	return static_cast<int>(CommandId::PaletteChange);
}

TileSheet::SubSheetIdx const&PaletteChangeCommand::subsheetIdx() const noexcept {
	return m_idx;
}

}
