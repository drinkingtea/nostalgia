/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <turbine/clipboard.hpp>

#include "commands.hpp"

namespace nostalgia::core {

class TileSheetClipboard: public turbine::ClipboardObject<TileSheetClipboard> {
	public:
		static constexpr auto TypeName = "net.drinkingtea.nostalgia.core.studio.TileSheetClipboard";
		static constexpr auto TypeVersion = 1;

		oxModelFriend(TileSheetClipboard);

		struct Pixel {
			static constexpr auto TypeName = "net.drinkingtea.nostalgia.core.studio.TileSheetClipboard.Pixel";
			static constexpr auto TypeVersion = 1;
			uint16_t colorIdx = 0;
			ox::Point pt;
			Pixel(uint16_t pColorIdx, ox::Point pPt) noexcept;
		};
	protected:
		ox::Vector<Pixel> m_pixels;

	public:
		void addPixel(ox::Point const&pt, uint16_t colorIdx) noexcept;

		[[nodiscard]]
		ox::Vector<Pixel> const&pixels() const noexcept;
};

oxModelBegin(TileSheetClipboard::Pixel)
	oxModelField(colorIdx)
	oxModelField(pt)
oxModelEnd()

oxModelBegin(TileSheetClipboard)
	oxModelFieldRename(pixels, m_pixels)
oxModelEnd()

class CutPasteCommand: public TileSheetCommand {
	private:
		struct Change {
			uint32_t idx = 0;
			uint16_t newPalIdx = 0;
			uint16_t oldPalIdx = 0;
			constexpr Change(uint32_t pIdx, uint16_t pNewPalIdx, uint16_t pOldPalIdx) noexcept {
				idx = pIdx;
				newPalIdx = pNewPalIdx;
				oldPalIdx = pOldPalIdx;
			}
		};
		CommandId m_commandId;
		TileSheet &m_img;
		TileSheet::SubSheetIdx m_subSheetIdx;
		ox::Vector<Change> m_changes;

	public:
		CutPasteCommand(
				CommandId commandId,
				TileSheet &img,
				TileSheet::SubSheetIdx subSheetIdx,
				ox::Point const&dstStart,
				ox::Point const&dstEnd,
				TileSheetClipboard const&cb) noexcept;

		void redo() noexcept final;

		void undo() noexcept final;

		[[nodiscard]]
		int commandId() const noexcept final;

		[[nodiscard]]
		TileSheet::SubSheetIdx const&subsheetIdx() const noexcept override;

};

}