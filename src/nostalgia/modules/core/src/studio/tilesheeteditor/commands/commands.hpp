/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <studio/undostack.hpp>
#include <nostalgia/core/tilesheet.hpp>

namespace nostalgia::core {

// Command IDs to use with QUndoCommand::id()
enum class CommandId {
	Draw = 1,
	AddSubSheet = 2,
	RmSubSheet = 3,
	DeleteTile = 4,
	InsertTile = 4,
	UpdateSubSheet = 5,
	Cut = 6,
	Paste = 7,
	PaletteChange = 8,
};

constexpr bool operator==(CommandId c, int i) noexcept {
	return static_cast<int>(c) == i;
}

constexpr bool operator==(int i, CommandId c) noexcept {
	return static_cast<int>(c) == i;
}

class TileSheetCommand: public studio::UndoCommand {
	public:
		[[nodiscard]]
		virtual TileSheet::SubSheetIdx const&subsheetIdx() const noexcept = 0;
};

}