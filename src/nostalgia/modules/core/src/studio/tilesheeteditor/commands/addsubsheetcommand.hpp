/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include "commands.hpp"

namespace nostalgia::core {

class AddSubSheetCommand: public TileSheetCommand {
	private:
		TileSheet &m_img;
		TileSheet::SubSheetIdx m_parentIdx;
		ox::Vector<TileSheet::SubSheetIdx, 2> m_addedSheets;

	public:
		AddSubSheetCommand(TileSheet &img, TileSheet::SubSheetIdx parentIdx) noexcept;

		void redo() noexcept final;

		void undo() noexcept final;

		[[nodiscard]]
		int commandId() const noexcept final;

		[[nodiscard]]
		TileSheet::SubSheetIdx const&subsheetIdx() const noexcept override;

};

}