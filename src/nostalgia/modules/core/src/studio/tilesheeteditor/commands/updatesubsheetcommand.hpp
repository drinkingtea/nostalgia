/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include "commands.hpp"

namespace nostalgia::core {

class UpdateSubSheetCommand: public TileSheetCommand {
	private:
		TileSheet &m_img;
		TileSheet::SubSheetIdx m_idx;
		TileSheet::SubSheet m_sheet;
		ox::String m_newName;
		int m_newCols = 0;
		int m_newRows = 0;

	public:
		UpdateSubSheetCommand(
				TileSheet &img,
				TileSheet::SubSheetIdx idx,
				ox::String name,
				int cols,
				int rows) noexcept;

		void redo() noexcept final;

		void undo() noexcept final;

		[[nodiscard]]
		int commandId() const noexcept final;

		[[nodiscard]]
		TileSheet::SubSheetIdx const&subsheetIdx() const noexcept override;

};

}