/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include "updatesubsheetcommand.hpp"

namespace nostalgia::core {

core::UpdateSubSheetCommand::UpdateSubSheetCommand(
		TileSheet &img,
		TileSheet::SubSheetIdx idx,
		ox::String name,
		int cols,
		int rows) noexcept:
		m_img(img),
		m_idx(std::move(idx)),
		m_sheet(m_img.getSubSheet(m_idx)),
		m_newName(std::move(name)),
		m_newCols(cols),
		m_newRows(rows) {
}

void UpdateSubSheetCommand::redo() noexcept {
	auto &sheet = m_img.getSubSheet(m_idx);
	sheet.name = m_newName;
	sheet.columns = m_newCols;
	sheet.rows = m_newRows;
	oxLogError(sheet.setPixelCount(m_img.bpp, static_cast<std::size_t>(PixelsPerTile * m_newCols * m_newRows)));
}

void UpdateSubSheetCommand::undo() noexcept {
	auto &sheet = m_img.getSubSheet(m_idx);
	sheet = m_sheet;
}

int UpdateSubSheetCommand::commandId() const noexcept {
	return static_cast<int>(CommandId::UpdateSubSheet);
}

TileSheet::SubSheetIdx const&UpdateSubSheetCommand::subsheetIdx() const noexcept {
	return m_idx;
}

}
