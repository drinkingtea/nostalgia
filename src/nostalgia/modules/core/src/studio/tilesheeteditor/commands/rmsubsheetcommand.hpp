/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include "commands.hpp"

namespace nostalgia::core {

class RmSubSheetCommand: public TileSheetCommand {
	private:
		TileSheet &m_img;
		TileSheet::SubSheetIdx m_idx;
		TileSheet::SubSheetIdx m_parentIdx;
		TileSheet::SubSheet m_sheet;

	public:
		RmSubSheetCommand(TileSheet &img, TileSheet::SubSheetIdx idx) noexcept;

		void redo() noexcept final;

		void undo() noexcept final;

		[[nodiscard]]
		int commandId() const noexcept final;

		[[nodiscard]]
		TileSheet::SubSheetIdx const&subsheetIdx() const noexcept override;

};

}