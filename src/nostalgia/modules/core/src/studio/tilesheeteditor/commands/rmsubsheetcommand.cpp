/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include "rmsubsheetcommand.hpp"

namespace nostalgia::core {

core::RmSubSheetCommand::RmSubSheetCommand(TileSheet &img, TileSheet::SubSheetIdx idx) noexcept:
		m_img(img),
		m_idx(std::move(idx)),
		m_parentIdx(m_idx) {
	m_parentIdx.pop_back();
	auto &parent = m_img.getSubSheet(m_parentIdx);
	m_sheet = parent.subsheets[*m_idx.back().value];
}

void RmSubSheetCommand::redo() noexcept {
	auto &parent = m_img.getSubSheet(m_parentIdx);
	oxLogError(parent.subsheets.erase(*m_idx.back().value).error);
}

void RmSubSheetCommand::undo() noexcept {
	auto &parent = m_img.getSubSheet(m_parentIdx);
	auto i = *m_idx.back().value;
	parent.subsheets.insert(i, m_sheet);
}

int RmSubSheetCommand::commandId() const noexcept {
	return static_cast<int>(CommandId::RmSubSheet);
}

TileSheet::SubSheetIdx const&RmSubSheetCommand::subsheetIdx() const noexcept {
	return m_idx;
}

}
