/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include "commands.hpp"

namespace nostalgia::core {

class PaletteChangeCommand: public TileSheetCommand {
	private:
		TileSheet &m_img;
		TileSheet::SubSheetIdx m_idx;
		ox::FileAddress m_oldPalette;
		ox::FileAddress m_newPalette;

	public:
		PaletteChangeCommand(
				TileSheet::SubSheetIdx idx,
				TileSheet &img,
				ox::CRStringView newPalette) noexcept;

		void redo() noexcept final;

		void undo() noexcept final;

		[[nodiscard]]
		int commandId() const noexcept final;

		[[nodiscard]]
		TileSheet::SubSheetIdx const&subsheetIdx() const noexcept override;

};

}