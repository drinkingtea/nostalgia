/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include "commands.hpp"

namespace nostalgia::core {

class InsertTilesCommand: public TileSheetCommand {
	private:
		TileSheet &m_img;
		TileSheet::SubSheetIdx m_idx;
		std::size_t m_insertPos = 0;
		std::size_t m_insertCnt = 0;
		ox::Vector<uint8_t> m_deletedPixels = {};

	public:
		InsertTilesCommand(
				TileSheet &img,
				TileSheet::SubSheetIdx idx,
				std::size_t tileIdx,
				std::size_t tileCnt) noexcept;

		void redo() noexcept final;

		void undo() noexcept final;

		[[nodiscard]]
		int commandId() const noexcept final;

		[[nodiscard]]
		TileSheet::SubSheetIdx const&subsheetIdx() const noexcept override;

};

}