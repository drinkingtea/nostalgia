/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include "commands.hpp"

namespace nostalgia::core {

class DrawCommand: public TileSheetCommand {
	private:
		struct Change {
			uint32_t idx = 0;
			uint16_t oldPalIdx = 0;
			constexpr Change(uint32_t pIdx, uint16_t pOldPalIdx) noexcept {
				idx = pIdx;
				oldPalIdx = pOldPalIdx;
			}
		};
		TileSheet &m_img;
		TileSheet::SubSheetIdx m_subSheetIdx;
		ox::Vector<Change, 8> m_changes;
		int m_palIdx = 0;

	public:
		DrawCommand(
				TileSheet &img,
				TileSheet::SubSheetIdx subSheetIdx,
				std::size_t idx,
				int palIdx) noexcept;

		DrawCommand(
				TileSheet &img,
				TileSheet::SubSheetIdx subSheetIdx,
				const ox::Vector<std::size_t> &idxList,
				int palIdx) noexcept;

		bool append(std::size_t idx) noexcept;

		bool append(const ox::Vector<std::size_t> &idxList) noexcept;

		void redo() noexcept final;

		void undo() noexcept final;

		[[nodiscard]]
		int commandId() const noexcept final;

		[[nodiscard]]
		TileSheet::SubSheetIdx const&subsheetIdx() const noexcept override;

};

}