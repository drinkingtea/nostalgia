/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <ox/std/vec.hpp>

#include <glutils/glutils.hpp>
#include <studio/studio.hpp>

#include <nostalgia/core/color.hpp>
#include <nostalgia/core/gfx.hpp>

namespace nostalgia::core {

class TileSheetPixels {

	private:
		static constexpr auto VertexVboRows = 4;
		static constexpr auto VertexVboRowLength = 5;
		static constexpr auto VertexVboLength = VertexVboRows * VertexVboRowLength;
		static constexpr auto VertexEboLength = 6;
		static constexpr auto VShad = R"(
		{}
		in vec2 vPosition;
		in vec3 vColor;
		out vec3 fColor;
		uniform vec2 vScroll;
		void main() {
			gl_Position = vec4(vPosition + vScroll, 0.0, 1.0);
			fColor = vColor;
		})";
		static constexpr auto FShad = R"(
		{}
		in vec3 fColor;
		out vec4 outColor;
		void main() {
			//outColor = vec4(0.0, 0.7, 1.0, 1.0);
			outColor = vec4(fColor, 1.0);
		})";

		float m_pixelSizeMod = 1;
		glutils::GLProgram m_shader;
		glutils::BufferSet m_bufferSet;
		const class TileSheetEditorModel *m_model = nullptr;

	public:
		explicit TileSheetPixels(class TileSheetEditorModel *model) noexcept;

		void setPixelSizeMod(float sm) noexcept;

		ox::Error buildShader() noexcept;

		void draw(bool update, ox::Vec2 const&scroll) noexcept;

		void initBufferSet(ox::Vec2 const&paneSize) noexcept;

		void update(ox::Vec2 const&paneSize) noexcept;

		[[nodiscard]]
		ox::Vec2 pixelSize(ox::Vec2 const&paneSize) const noexcept;

	private:
		void setPixelBufferObject(ox::Vec2 const&paneS, unsigned vertexRow, float x, float y, Color16 color, float *vbo, GLuint *ebo) const noexcept;

		void setBufferObjects(ox::Vec2 const&paneS) noexcept;

};

}
