add_library(NostalgiaCore-Studio)

add_library(
	NostalgiaCore-Studio-ImGui
		studiomodule.cpp
		tilesheeteditor/tilesheeteditor-imgui.cpp
)

target_link_libraries(
	NostalgiaCore-Studio PUBLIC
		NostalgiaCore
		Studio
)

target_link_libraries(
	NostalgiaCore-Studio-ImGui PUBLIC
		NostalgiaCore-Studio
		Studio
)

install(
	TARGETS
		NostalgiaCore-Studio-ImGui
		NostalgiaCore-Studio
	LIBRARY DESTINATION
		${NOSTALGIA_DIST_MODULE}
)

add_subdirectory(paletteeditor)
add_subdirectory(tilesheeteditor)
