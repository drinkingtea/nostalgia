/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <ox/std/memory.hpp>

#include <studio/studio.hpp>

#include "paletteeditor/paletteeditor-imgui.hpp"
#include "tilesheeteditor/tilesheeteditor-imgui.hpp"

namespace nostalgia::core {

class StudioModule: public studio::Module {
	ox::Vector<studio::EditorMaker> editors(turbine::Context &ctx) const noexcept final {
		return {
			studio::editorMaker<TileSheetEditorImGui>(ctx, FileExt_ng),
			studio::editorMaker<PaletteEditorImGui>(ctx, FileExt_npal),
		};
	}

	ox::Vector<ox::UPtr<studio::ItemMaker>> itemMakers(turbine::Context&) const noexcept final {
		ox::Vector<ox::UniquePtr<studio::ItemMaker>> out;
		out.emplace_back(ox::make<studio::ItemMakerT<core::TileSheet>>("Tile Sheet", "TileSheets", FileExt_ng));
		out.emplace_back(ox::make<studio::ItemMakerT<core::Palette>>("Palette", "Palettes", FileExt_npal));
		return out;
	}
};

static StudioModule mod;
const studio::Module *studioModule() noexcept {
	return &mod;
}

}
