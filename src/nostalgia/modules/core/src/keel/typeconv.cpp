/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include "typeconv.hpp"

namespace nostalgia::core {

ox::Error NostalgiaPaletteToPaletteConverter::convert(
		keel::Context&,
		NostalgiaPalette &src,
		Palette &dst) const noexcept {
	dst.colors = std::move(src.colors);
	return {};
}

ox::Error TileSheetV1ToTileSheetV2Converter::convert(
		keel::Context&,
		TileSheetV1 &src,
		TileSheetV2 &dst) const noexcept {
	dst.bpp              = src.bpp;
	dst.defaultPalette   = std::move(src.defaultPalette);
	dst.subsheet.name    = "Root";
	dst.subsheet.rows    = src.rows;
	dst.subsheet.columns = src.columns;
	dst.subsheet.pixels  = std::move(src.pixels);
	return {};
}

void TileSheetV2ToTileSheetV3Converter::convertSubsheet(
		TileSheetV2::SubSheet &src,
		TileSheetV3::SubSheet &dst,
		SubSheetId &idIt) noexcept {
	dst.id      = idIt;
	dst.name    = std::move(src.name);
	dst.columns = src.columns;
	dst.rows    = src.rows;
	dst.pixels  = std::move(src.pixels);
	++idIt;
	dst.subsheets.resize(src.subsheets.size());
	for (auto i = 0u; i < src.subsheets.size(); ++i) {
		convertSubsheet(src.subsheets[i], dst.subsheets[i], idIt);
	}
}

ox::Error TileSheetV2ToTileSheetV3Converter::convert(
		keel::Context&,
		TileSheetV2 &src,
		TileSheetV3 &dst) const noexcept {
	dst.bpp            = src.bpp;
	dst.defaultPalette = std::move(src.defaultPalette);
	convertSubsheet(src.subsheet, dst.subsheet, dst.idIt);
	return {};
}

ox::Error TileSheetToCompactTileSheetConverter::convert(
		keel::Context&,
		TileSheet &src,
		CompactTileSheet &dst) const noexcept {
	dst.bpp            = src.bpp;
	dst.defaultPalette = std::move(src.defaultPalette);
	dst.pixels         = src.pixels();
	return {};
}

}
