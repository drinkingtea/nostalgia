/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <ox/std/def.hpp>

#include <keel/typeconv.hpp>

#include <nostalgia/core/context.hpp>
#include <nostalgia/core/palette.hpp>
#include <nostalgia/core/tilesheet.hpp>

namespace nostalgia::core {

// Type converters

class NostalgiaPaletteToPaletteConverter: public keel::Converter<NostalgiaPalette, Palette> {
	ox::Error convert(keel::Context&, NostalgiaPalette &src, Palette &dst) const noexcept final;
};

class TileSheetV1ToTileSheetV2Converter: public keel::Converter<TileSheetV1, TileSheetV2> {
	ox::Error convert(keel::Context&, TileSheetV1 &src, TileSheetV2 &dst) const noexcept final;
};

class TileSheetV2ToTileSheetV3Converter: public keel::Converter<TileSheetV2, TileSheetV3> {
	static void convertSubsheet(
			TileSheetV2::SubSheet &src,
			TileSheetV3::SubSheet &dst,
			SubSheetId &idIt) noexcept;
	ox::Error convert(keel::Context&, TileSheetV2 &src, TileSheetV3 &dst) const noexcept final;
};

class TileSheetToCompactTileSheetConverter: public keel::Converter<TileSheet, CompactTileSheet> {
		ox::Error convert(keel::Context&, TileSheet &src, CompactTileSheet &dst) const noexcept final;
};

}
