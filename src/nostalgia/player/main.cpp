/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <ox/logconn/def.hpp>
#include <ox/logconn/logconn.hpp>

#include <keel/media.hpp>

#include "../modules/keelmodules.hpp"

#include "app.hpp"

static ox::Error run(int argc, const char **argv) noexcept {
#ifndef OX_BARE_METAL
	// GBA doesn't need the modules and calling this doubles the size of the
	// binary.
	nostalgia::registerKeelModules();
#endif
	if (argc < 2) {
		return OxError(1, "Please provide path to project directory or OxFS file.");
	}
	const auto path = argv[1];
	oxRequireM(fs, keel::loadRomFs(path));
	return run(std::move(fs));
}

#ifdef _WIN32
int WinMain() {
	auto const argc = __argc;
	auto const argv = const_cast<const char**>(__argv);
#else
int main(int argc, const char **argv) {
#endif
	OX_INIT_DEBUG_LOGGER(loggerConn, "Nostalgia Player")
	ox::Error err;
	err = run(argc, argv);
	oxAssert(err, "Something went wrong...");
	if (err) {
		oxErrf("Failure: {}\n", toStr(err));
	}
	return static_cast<int>(err);
}
