/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <ox/std/memory.hpp>
#include <ox/fs/fs.hpp>

typename ox::Error run(ox::UniquePtr<ox::FileSystem> &&fs) noexcept;
