/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <keel/media.hpp>
#include <turbine/turbine.hpp>

#include <nostalgia/core/core.hpp>
#include <nostalgia/scene/scene.hpp>

using namespace nostalgia;

static bool s_paused = false;
static ox::Optional<scene::Scene> s_scene;

static int updateHandler(turbine::Context&) noexcept {
	constexpr auto sleepTime = 16;
	if (s_paused) {
		return sleepTime;
	}
	// do stuff
	return sleepTime;
}

static void keyEventHandler(turbine::Context &tctx, turbine::Key key, bool down) noexcept {
	if (down) {
		if (key == turbine::Key::Alpha_Q) {
			turbine::requestShutdown(tctx);
		} else if (key == turbine::Key::Alpha_P) {
			s_paused = !s_paused;
		}
	}
}

ox::Error run(ox::UniquePtr<ox::FileSystem> &&fs) noexcept {
	oxRequireM(tctx, turbine::init(std::move(fs), "Nostalgia"));
	oxRequireM(cctx, core::init(*tctx));
	constexpr ox::FileAddress SceneAddr = ox::StringLiteral("/Scenes/Chester.nscn");
	oxRequire(scn, keel::readObj<scene::SceneStatic>(keelCtx(*tctx), SceneAddr));
	turbine::setUpdateHandler(*tctx, updateHandler);
	turbine::setKeyEventHandler(*tctx, keyEventHandler);
	s_scene.emplace(*scn);
	oxReturnError(s_scene->setupDisplay(*cctx));
	return turbine::run(*tctx);
}
