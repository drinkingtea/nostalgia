/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <ox/std/defines.hpp>

namespace turbine {

enum Key {
	// GBA implementation currently relies on GamePad entry order
	GamePad_A = 0,
	GamePad_B,
	GamePad_Select,
	GamePad_Start,
	GamePad_Right,
	GamePad_Left,
	GamePad_Up,
	GamePad_Down,
	GamePad_R,
	GamePad_L,

	Num_0,
	Num_1,
	Num_2,
	Num_3,
	Num_4,
	Num_5,
	Num_6,
	Num_7,
	Num_8,
	Num_9,

	Alpha_A,
	Alpha_B,
	Alpha_C,
	Alpha_D,
	Alpha_E,
	Alpha_F,
	Alpha_G,
	Alpha_H,
	Alpha_I,
	Alpha_J,
	Alpha_K,
	Alpha_L,
	Alpha_M,
	Alpha_N,
	Alpha_O,
	Alpha_P,
	Alpha_Q,
	Alpha_R,
	Alpha_S,
	Alpha_T,
	Alpha_U,
	Alpha_V,
	Alpha_W,
	Alpha_X,
	Alpha_Y,
	Alpha_Z,

	Mod_Alt,
	Mod_Ctrl,
	Mod_Super,
	Mod_Shift,

	Escape,

	End
};

class Context;

[[nodiscard]]
bool buttonDown(Context const&ctx, Key) noexcept;

using KeyEventHandler = void(*)(Context&, Key, bool);

}
