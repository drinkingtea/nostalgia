/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <ox/std/array.hpp>
#include <ox/std/point.hpp>
#include <ox/std/size.hpp>
#include <ox/std/types.hpp>
#include <ox/model/def.hpp>

#include "context.hpp"

namespace turbine {

namespace gl {
class Drawer {
	public:
		virtual ~Drawer() = default;
		virtual void draw(Context&) noexcept = 0;
};
void addDrawer(Context &ctx, Drawer *cd) noexcept;
void removeDrawer(Context &ctx, Drawer *cd) noexcept;
}

ox::Error initGfx(Context &ctx) noexcept;

void setWindowTitle(Context &ctx, ox::CRStringView title) noexcept;

void focusWindow(Context &ctx) noexcept;

[[nodiscard]]
int getScreenWidth(Context &ctx) noexcept;

[[nodiscard]]
int getScreenHeight(Context &ctx) noexcept;

[[nodiscard]]
ox::Size getScreenSize(Context &ctx) noexcept;

ox::Bounds getWindowBounds(Context &ctx) noexcept;

ox::Error setWindowBounds(Context &ctx, ox::Bounds const&bnds) noexcept;

void setConstantRefresh(Context &ctx, bool r) noexcept;

}
