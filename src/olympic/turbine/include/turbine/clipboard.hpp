/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <ox/claw/claw.hpp>
#include <ox/std/memory.hpp>
#include <ox/std/string.hpp>

#include "context.hpp"

namespace turbine {

class BaseClipboardObject {
	public:
		virtual ~BaseClipboardObject() noexcept = default;

		[[nodiscard]]
		virtual ox::String typeId() const noexcept = 0;

		[[nodiscard]]
		constexpr auto typeMatch(ox::StringView name, int version) const noexcept {
			return typeId() == ox::buildTypeId(name, version);
		}
};

template<typename T>
class ClipboardObject: public BaseClipboardObject {
	[[nodiscard]]
	ox::String typeId() const noexcept final {
		return ox::buildTypeId(T::TypeName, T::TypeVersion);
	}
};

ox::String getClipboardText(Context &ctx) noexcept;

void setClipboardText(Context &ctx, ox::CRStringView text) noexcept;

void setClipboardObject(Context &ctx, ox::UniquePtr<BaseClipboardObject> &&obj) noexcept;

ox::Result<BaseClipboardObject*> getClipboardData(Context &ctx, ox::StringView typeName, int typeVersion) noexcept;

template<typename T>
ox::Result<T*> getClipboardObject(Context &ctx) noexcept {
	oxRequire(p, getClipboardData(ctx, T::TypeName, T::TypeVersion));
	return dynamic_cast<T*>(p);
}

}
