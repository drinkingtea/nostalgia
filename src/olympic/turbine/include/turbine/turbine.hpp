/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <ox/std/memory.hpp>
#include <ox/fs/fs.hpp>

#include "clipboard.hpp"
#include "event.hpp"
#include "gfx.hpp"
#include "input.hpp"

namespace turbine {

ox::Result<ContextUPtr> init(ox::UPtr<ox::FileSystem> &&fs, ox::CRStringView appName) noexcept;

ox::Error run(Context &ctx) noexcept;

// Returns the number of milliseconds that have passed since the start of the
// program.
[[nodiscard]]
uint64_t ticksMs(Context const&ctx) noexcept;

void requestShutdown(Context &ctx) noexcept;

}
