/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <ox/fs/fs.hpp>
#include <ox/model/desctypes.hpp>
#include <ox/std/buffer.hpp>
#include <ox/std/size.hpp>

#include <keel/context.hpp>

#include "input.hpp"

namespace turbine {

class Context;

struct ContextDeleter {
	void operator()(Context *p) noexcept;
};

using ContextUPtr = ox::UPtr<Context, ContextDeleter>;

void shutdown(Context &ctx) noexcept;

keel::Context const&keelCtx(Context const&ctx) noexcept;

keel::Context &keelCtx(Context &ctx) noexcept;

inline ox::FileSystem const*rom(Context const&ctx) noexcept {
	return keelCtx(ctx).rom.get();
}

inline ox::FileSystem *rom(Context &ctx) noexcept {
	return keelCtx(ctx).rom.get();
}

void setApplicationData(Context &ctx, void *applicationData) noexcept;

[[nodiscard]]
void *applicationDataRaw(Context &ctx) noexcept;

template<typename T>
[[nodiscard]]
T *applicationData(Context &ctx) noexcept {
	return static_cast<T*>(applicationDataRaw(ctx));
}

void setKeyEventHandler(Context &ctx, KeyEventHandler h) noexcept;

KeyEventHandler keyEventHandler(Context &ctx) noexcept;

}

