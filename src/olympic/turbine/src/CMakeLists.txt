add_library(Turbine)

set(TURBINE_BACKEND_GBA ${TURBINE_BUILD_TYPE} STREQUAL "GBA")
set(TURBINE_BACKEND_GLFW NOT ${TURBINE_BACKEND_GBA})

add_subdirectory(gba)
if(${TURBINE_BACKEND_GLFW})
	add_subdirectory(glfw)
endif()

target_include_directories(
	Turbine PUBLIC
		../include
)

target_link_libraries(
	Turbine PUBLIC
		Keel
)

target_compile_definitions(
	Turbine PRIVATE
		TURBINE_BACKEND_GBA=$<IF:$<BOOL:${TURBINE_BACKEND_GBA}>,1,0>
		TURBINE_BACKEND_GLFW=$<IF:$<BOOL:${TURBINE_BACKEND_GLFW}>,1,0>
)

install(
	DIRECTORY
		../include/turbine
	DESTINATION
		include/turbine
)

install(
	TARGETS
		Turbine
	DESTINATION
		LIBRARY DESTINATION lib
		ARCHIVE DESTINATION lib
)
