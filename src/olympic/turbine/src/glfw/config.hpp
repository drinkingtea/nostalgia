/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

namespace turbine::config {

constexpr bool GlFpsPrint = TURBINE_GL_FPS_PRINT;

}
