/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <turbine/event.hpp>

#include "context.hpp"

namespace turbine {

void setUpdateHandler(Context &ctx, UpdateHandler h) noexcept {
	ctx.updateHandler = h;
}

void setKeyEventHandler(Context &ctx, KeyEventHandler h) noexcept {
	ctx.keyEventHandler = h;
}

KeyEventHandler keyEventHandler(Context &ctx) noexcept {
	return ctx.keyEventHandler;
}

}
