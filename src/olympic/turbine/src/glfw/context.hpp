/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <turbine/clipboard.hpp>
#include <turbine/context.hpp>
#include <turbine/gfx.hpp>
#include <turbine/event.hpp>

namespace turbine {

class Context {
	public:
		UpdateHandler updateHandler = [](Context&) -> int {return 0;};
		keel::Context keelCtx;
		KeyEventHandler keyEventHandler = nullptr;
		void *applicationData = nullptr;

		// GLFW impl data ////////////////////////////////////////////////////////
		int uninterruptedRefreshes = 3;
		ox::UPtr<BaseClipboardObject> clipboard;
		struct GLFWwindow *window = nullptr;
		// sets screen refresh to constant instead of only on event
		bool constantRefresh = true;
		ox::Vector<gl::Drawer*, 5> drawers;
		int64_t startTime = 0;
		uint64_t wakeupTime = 0;
		uint64_t keysDown = 0;
		uint64_t prevFpsCheckTime = 0;
		uint64_t draws = 0;

		Context() noexcept = default;

		Context(Context const&other) noexcept = delete;
		Context(Context const&&other) noexcept = delete;

};

}
