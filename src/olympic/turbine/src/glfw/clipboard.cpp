/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <GLFW/glfw3.h>

#include <ox/std/string.hpp>

#include <turbine/turbine.hpp>

#include "context.hpp"

namespace turbine {

ox::String getClipboardText(Context &ctx) noexcept {
	return ox::String(glfwGetClipboardString(ctx.window));
}

void setClipboardText(Context &ctx, ox::CRStringView text) noexcept {
	auto cstr = ox_malloca(text.bytes() + 1, char);
	ox_strncpy(cstr.get(), text.data(), text.bytes());
	glfwSetClipboardString(ctx.window, cstr.get());
}

void setClipboardObject(Context &ctx, ox::UniquePtr<BaseClipboardObject> &&obj) noexcept {
	ctx.clipboard = std::move(obj);
}

ox::Result<BaseClipboardObject*> getClipboardData(Context &ctx, ox::StringView typeName, int typeVersion) noexcept {
	if (ctx.clipboard && ctx.clipboard->typeMatch(typeName, typeVersion)) {
		return ctx.clipboard.get();
	}
	return OxError(1);
}

}
