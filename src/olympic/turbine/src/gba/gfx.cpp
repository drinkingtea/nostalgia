/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <ox/std/size.hpp>
#include <ox/std/stringview.hpp>

#include <teagba/addresses.hpp>
#include <teagba/gfx.hpp>
#include <teagba/irq.hpp>

#include <turbine/context.hpp>

namespace turbine {

ox::Error initGfx(Context&) noexcept {
	REG_DISPCTL = teagba::DispCtl_Mode0
	            | teagba::DispCtl_SpriteMap1D
	            | teagba::DispCtl_Obj;
	// tell display to trigger vblank interrupts
	REG_DISPSTAT = REG_DISPSTAT | teagba::DispStat_irq_vblank;
	// enable vblank interrupt
	REG_IE = REG_IE | teagba::Int_vblank;
	return {};
}

void setWindowTitle(Context&, ox::CRStringView) noexcept {
}

int getScreenWidth(Context&) noexcept {
	return 240;
}

int getScreenHeight(Context&) noexcept {
	return 160;
}

ox::Size getScreenSize(Context&) noexcept {
	return {240, 160};
}

ox::Bounds getWindowBounds(Context&) noexcept {
	return {0, 0, 240, 160};
}

ox::Error setWindowBounds(Context&, ox::Bounds const&) noexcept {
	return OxError(1, "setWindowBounds not supported on GBA");
}

}
