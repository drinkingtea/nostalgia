/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <ox/std/types.hpp>

#include "config.hpp"

namespace turbine {

using gba_timer_t = ox::Uint<config::GbaTimerBits>;

}
