/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

namespace turbine::config {

constexpr bool GbaEventLoopTimerBased = TURBINE_GBA_EVENT_LOOP_TIMER_BASED;
constexpr int GbaTimerBits = TURBINE_GBA_TIMER_BITS;

}
