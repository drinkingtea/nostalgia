/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

// stub for building TeaGBA for PC targets, for purposes of not having to
// switch back and forth between builds when editing GBA files

extern "C" void turbine_isr() {}
