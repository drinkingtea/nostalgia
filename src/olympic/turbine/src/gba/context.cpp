/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include "context.hpp"

namespace turbine {

void ContextDeleter::operator()(Context *p) noexcept {
	ox::safeDelete(p);
}

keel::Context const&keelCtx(Context const&ctx) noexcept {
	return ctx.keelCtx;
}

keel::Context &keelCtx(Context &ctx) noexcept {
	return ctx.keelCtx;
}

void setApplicationData(Context &ctx, void *applicationData) noexcept {
	ctx.applicationData = applicationData;
}

void *applicationDataRaw(Context &ctx) noexcept {
	return ctx.applicationData;
}

}
