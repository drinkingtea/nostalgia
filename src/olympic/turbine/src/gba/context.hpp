/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <keel/context.hpp>

#include <turbine/clipboard.hpp>
#include <turbine/context.hpp>
#include <turbine/event.hpp>

namespace turbine {

class Context {
	public:
		UpdateHandler updateHandler = [](Context&) -> int {return 0;};
		keel::Context keelCtx;
		KeyEventHandler keyEventHandler = nullptr;
		void *applicationData = nullptr;

		// GBA impl data /////////////////////////////////////////////////////////
		bool running = true;

		Context() noexcept = default;
		Context(Context &other) noexcept = delete;
		Context(Context const&other) noexcept = delete;
		Context(Context const&&other) noexcept = delete;

		virtual inline ~Context() noexcept {
			shutdown(*this);
		}

};

}
