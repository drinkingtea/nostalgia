/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <ox/std/string.hpp>

#include <turbine/context.hpp>

namespace turbine {

ox::String getClipboardText(Context&) noexcept {
	return {};
}

void setClipboardText(Context&, ox::CRStringView) noexcept {
}

}
