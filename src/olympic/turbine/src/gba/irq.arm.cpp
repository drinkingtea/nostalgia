/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

// NOTE: this file is compiled as ARM and not THUMB, so don't but too much in
// here

#include <teagba/addresses.hpp>
#include <teagba/gfx.hpp>
#include <teagba/irq.hpp>

#include "turbine.hpp"

namespace turbine {

volatile gba_timer_t g_timerMs = 0;

}

using namespace turbine;

extern "C" {

void turbine_isr_vblank() noexcept {
	teagba::applySpriteUpdates();
	if constexpr(config::GbaEventLoopTimerBased) {
		// disable vblank interrupt until it is needed again
		REG_IE = REG_IE & ~teagba::Int_vblank;
	}
}

void turbine_isr_timer0() noexcept {
	g_timerMs = g_timerMs + 1;
}

}
