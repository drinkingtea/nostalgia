/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <ox/event/signal.hpp>

#include "project.hpp"

namespace studio {

struct StudioContext {
	ox::SignalHandler *ui = nullptr;
	Project *project = nullptr;
};

}
