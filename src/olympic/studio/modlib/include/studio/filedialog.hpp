/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <ox/std/defines.hpp>

namespace studio {

struct FDFilterItem {
#ifdef OX_OS_Windows
	using String = ox::Vector<wchar_t>;
#else
	using String = ox::Vector<char>;
#endif
	String name{};
	String spec{};
	constexpr FDFilterItem() noexcept = default;
	FDFilterItem(ox::CRStringView pName, ox::CRStringView pSpec) noexcept;
};

ox::Result<ox::String> saveFile(ox::Vector<FDFilterItem> const&exts) noexcept;

ox::Result<ox::String> chooseDirectory() noexcept;

}