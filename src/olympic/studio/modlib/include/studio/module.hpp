/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <functional>

#include <ox/std/string.hpp>
#include <ox/std/vector.hpp>

#include <turbine/context.hpp>

#include <studio/itemmaker.hpp>

namespace studio {

class ItemMaker;

struct EditorMaker {
	using Func = std::function<ox::Result<class BaseEditor*>(ox::CRStringView)>;
	ox::Vector<ox::String> fileTypes;
	Func make;
};

class Module {
	public:
		virtual ~Module() noexcept = default;

		virtual ox::Vector<EditorMaker> editors(turbine::Context &ctx) const;

		virtual ox::Vector<ox::UPtr<ItemMaker>> itemMakers(turbine::Context&) const;

};

template<typename Editor>
[[nodiscard]]
studio::EditorMaker editorMaker(turbine::Context &ctx, ox::CRStringView ext) noexcept {
	return {
			{ox::String(ext)},
			[&ctx](ox::CRStringView path) -> ox::Result<studio::BaseEditor*> {
				return ox::makeCatch<Editor>(ctx, path);
			}
	};
}

}
