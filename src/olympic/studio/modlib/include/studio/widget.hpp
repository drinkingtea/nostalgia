/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <ox/event/signal.hpp>

#include <turbine/context.hpp>

namespace studio {

class Widget: public ox::SignalHandler {
	public:
		~Widget() noexcept override = default;
		virtual void draw(turbine::Context&) noexcept = 0;
};

}
