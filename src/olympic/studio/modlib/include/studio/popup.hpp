/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <functional>

#include <ox/event/signal.hpp>
#include <ox/std/string.hpp>
#include <ox/std/vec.hpp>

#include <turbine/context.hpp>

namespace studio {


class Popup {
	private:
		ox::Vec2 m_size;
		ox::String m_title;
	public:
		// emits path parameter
		ox::Signal<ox::Error(const ox::String&)> finished;

		virtual ~Popup() noexcept = default;

		virtual void open() noexcept = 0;

		virtual void close() noexcept = 0;

		[[nodiscard]]
		virtual bool isOpen() const noexcept = 0;

		virtual void draw(turbine::Context &ctx) noexcept = 0;

	protected:
		constexpr void setSize(ox::Size sz) noexcept {
			m_size = {static_cast<float>(sz.width), static_cast<float>(sz.height)};
		}

		constexpr void setTitle(ox::String title) noexcept {
			m_title = std::move(title);
		}

		constexpr const ox::String &title() const noexcept {
			return m_title;
		}

		void drawWindow(turbine::Context &ctx, bool *open, std::function<void()> const&drawContents);

};


}
