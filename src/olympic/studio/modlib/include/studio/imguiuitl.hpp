/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <imgui.h>

#include <turbine/context.hpp>

namespace studio::ig {

void centerNextWindow(turbine::Context &ctx) noexcept;

}