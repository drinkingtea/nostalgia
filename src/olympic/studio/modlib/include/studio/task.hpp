/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <ox/event/signal.hpp>

#include <turbine/context.hpp>

namespace studio {

enum class TaskState {
	Running,
	Done
};

class Task: public ox::SignalHandler {
	public:
		ox::Signal<ox::Error()> finished;
		~Task() noexcept override = default;
		virtual TaskState update(turbine::Context &ctx) noexcept = 0;
};

class TaskRunner {
	private:
		ox::Vector<ox::UniquePtr<studio::Task>> m_tasks;
	public:
		void update(turbine::Context &ctx) noexcept;
		void add(Task &task) noexcept;
};

}
