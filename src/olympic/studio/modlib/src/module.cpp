/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <studio/module.hpp>

namespace studio {

ox::Vector<EditorMaker> Module::editors(turbine::Context&) const {
	return {};
}

ox::Vector<ox::UPtr<ItemMaker>> Module::itemMakers(turbine::Context&) const {
	return {};
}

}
