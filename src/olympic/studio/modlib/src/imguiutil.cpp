/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <imgui.h>

#include <turbine/gfx.hpp>

namespace studio::ig {

void centerNextWindow(turbine::Context &ctx) noexcept {
	const auto sz = turbine::getScreenSize(ctx);
	const auto screenW = static_cast<float>(sz.width);
	const auto screenH = static_cast<float>(sz.height);
	const auto mod = ImGui::GetWindowDpiScale() * 2;
	ImGui::SetNextWindowPos(ImVec2(screenW / mod, screenH / mod), ImGuiCond_Always, ImVec2(0.5f, 0.5f));
}

}
