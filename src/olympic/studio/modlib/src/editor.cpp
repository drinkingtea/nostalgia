/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <algorithm>

#include <ox/std/string.hpp>

#include <studio/editor.hpp>

namespace studio {

ox::CStringView BaseEditor::itemDisplayName() const noexcept {
	return itemPath();
}

void BaseEditor::cut() {
}

void BaseEditor::copy() {
}

void BaseEditor::paste() {
}

void BaseEditor::exportFile() {
}

void BaseEditor::keyStateChanged(turbine::Key, bool) {
}

void BaseEditor::onActivated() noexcept {
}

bool BaseEditor::requiresConstantRefresh() const noexcept {
	return m_requiresConstantRefresh;
}

void BaseEditor::close() const {
	this->closed.emit(itemPath());
}

void BaseEditor::save() noexcept {
	const auto err = saveItem();
	if (!err) {
		setUnsavedChanges(false);
	} else {
		if constexpr(ox::defines::Debug) {
			oxErrorf("Could not save file {}: {} ({}:{})", itemPath(), toStr(err), err.file, err.line);
		} else {
			oxErrorf("Could not save file {}: {}", itemPath(), toStr(err));
		}
	}
}

void BaseEditor::setUnsavedChanges(bool uc) {
    m_unsavedChanges = uc;
    unsavedChangesChanged.emit(uc);
}

bool BaseEditor::unsavedChanges() const noexcept {
	return m_unsavedChanges;
}

void BaseEditor::setExportable(bool exportable) {
	m_exportable = exportable;
	exportableChanged.emit(exportable);
}

bool BaseEditor::exportable() const {
	return m_exportable;
}

void BaseEditor::setCutEnabled(bool v) {
	m_cutEnabled = v;
	cutEnabledChanged.emit(v);
}

bool BaseEditor::cutEnabled() const {
	return m_cutEnabled;
}

void BaseEditor::setCopyEnabled(bool v) {
	m_copyEnabled = v;
	copyEnabledChanged.emit(v);
}

bool BaseEditor::copyEnabled() const {
	return m_copyEnabled;
}

void BaseEditor::setPasteEnabled(bool v) {
	m_pasteEnabled = v;
	pasteEnabledChanged.emit(v);
}

bool BaseEditor::pasteEnabled() const {
	return m_pasteEnabled;
}

ox::Error BaseEditor::saveItem() noexcept {
	return OxError(0);
}

UndoStack *BaseEditor::undoStack() noexcept {
	return nullptr;
}

void BaseEditor::setRequiresConstantRefresh(bool value) noexcept {
	m_requiresConstantRefresh = value;
}


Editor::Editor(ox::StringView itemPath) noexcept:
		m_itemPath(itemPath),
		m_itemName(m_itemPath.substr(std::find(m_itemPath.rbegin(), m_itemPath.rend(), '/').offset() + 1)) {
	m_undoStack.changeTriggered.connect(this, &Editor::markUnsavedChanges);
}

[[nodiscard]]
ox::CStringView Editor::itemPath() const noexcept {
	return m_itemPath;
}

[[nodiscard]]
ox::CStringView Editor::itemDisplayName() const noexcept {
	return m_itemName;
}

UndoStack *Editor::undoStack() noexcept {
	return &m_undoStack;
}

ox::Error Editor::markUnsavedChanges(UndoCommand const*) noexcept {
	setUnsavedChanges(true);
	return {};
}

}
