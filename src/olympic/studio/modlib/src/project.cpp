/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <algorithm>
#include <filesystem>

#include <ox/std/std.hpp>

#include <keel/module.hpp>

#include <studio/project.hpp>

namespace studio {

static void generateTypes(ox::TypeStore &ts) noexcept {
	for (const auto mod : keel::modules()) {
		for (auto gen : mod->types()) {
			oxLogError(gen(ts));
		}
	}
}

Project::Project(keel::Context &ctx, ox::String path, ox::CRStringView projectDataDir) noexcept:
	m_ctx(ctx),
	m_path(std::move(path)),
	m_projectDataDir(projectDataDir),
	m_typeStore(*m_ctx.rom, ox::sfmt("/{}/type_descriptors", projectDataDir)),
	m_fs(*m_ctx.rom) {
	oxTracef("studio", "Project: {}", m_path);
	generateTypes(m_typeStore);
	buildFileIndex();
}

ox::Error Project::create() noexcept {
	std::error_code ec;
	std::filesystem::create_directory(m_path.toStdString(), ec);
	return OxError(static_cast<ox::ErrorCode>(ec.value()), "PassThroughFS: mkdir failed");
}

ox::FileSystem *Project::romFs() noexcept {
	return &m_fs;
}

ox::Error Project::mkdir(ox::CRStringView path) const noexcept {
	oxReturnError(m_fs.mkdir(path, true));
	fileUpdated.emit(path);
	return {};
}

ox::Result<ox::FileStat> Project::stat(ox::CRStringView path) const noexcept {
	return m_fs.stat(path);
}

bool Project::exists(ox::CRStringView path) const noexcept {
	return m_fs.stat(path).error == 0;
}

const ox::Vector<ox::String> &Project::fileList(ox::CRStringView ext) noexcept {
	return m_fileExtFileMap[ext];
}

void Project::buildFileIndex() noexcept {
	auto [files, err] = listFiles();
	if (err) {
		oxLogError(err);
		return;
	}
	m_fileExtFileMap.clear();
	std::sort(files.begin(), files.end());
	for (const auto &file : files) {
		if (!beginsWith(file, ox::sfmt("/.{}/", m_projectDataDir))) {
			indexFile(file);
		}
	}
}

void Project::indexFile(ox::CRStringView path) noexcept {
	const auto [ext, err] = fileExt(path);
	if (err) {
		return;
	}
	m_fileExtFileMap[ext].emplace_back(path);
}

ox::Error Project::writeBuff(ox::CRStringView path, ox::Buffer const&buff) noexcept {
	constexpr auto HdrSz = 40;
	ox::Buffer outBuff;
	outBuff.reserve(buff.size() + HdrSz);
	ox::BufferWriter writer(&outBuff);
	const auto [uuid, err] = m_ctx.pathToUuid.at(path);
	if (!err) {
		oxReturnError(keel::writeUuidHeader(writer, *uuid));
	}
	oxReturnError(writer.write(buff.data(), buff.size()));
	const auto newFile = m_fs.stat(path).error != 0;
	oxReturnError(m_fs.write(path, outBuff.data(), outBuff.size(), ox::FileType::NormalFile));
	if (newFile) {
		fileAdded.emit(path);
		indexFile(path);
	} else {
		fileUpdated.emit(path);
	}
	return {};
}

ox::Result<ox::Buffer> Project::loadBuff(ox::CRStringView path) const noexcept {
	return m_fs.read(path);
}

ox::Error Project::lsProcDir(ox::Vector<ox::String> *paths, ox::CRStringView path) const noexcept {
	oxRequire(files, m_fs.ls(path));
	for (const auto &name : files) {
		auto fullPath = ox::sfmt("{}/{}", path, name);
		oxRequire(stat, m_fs.stat(ox::StringView(fullPath)));
		switch (stat.fileType) {
			case ox::FileType::NormalFile:
				paths->emplace_back(std::move(fullPath));
				break;
			case ox::FileType::Directory:
				oxReturnError(lsProcDir(paths, fullPath));
				break;
			case ox::FileType::None:
				break;
		}
	}
	return {};
}

ox::Result<ox::Vector<ox::String>> Project::listFiles(ox::CRStringView path) const noexcept {
	ox::Vector<ox::String> paths;
	oxReturnError(lsProcDir(&paths, path));
	return paths;
}

}
