/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <algorithm>

#include <studio/task.hpp>

namespace studio {

void TaskRunner::update(turbine::Context &ctx) noexcept {
	oxIgnoreError(m_tasks.erase(std::remove_if(m_tasks.begin(), m_tasks.end(), [&](ox::UPtr<studio::Task> &t) {
		const auto done = t->update(ctx) == TaskState::Done;
		if (done) {
			t->finished.emit();
		}
		return done;
	})));
}

void TaskRunner::add(Task &task) noexcept {
	m_tasks.emplace_back(&task);
}

}
