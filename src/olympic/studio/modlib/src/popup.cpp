/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <studio/imguiuitl.hpp>
#include <studio/popup.hpp>

namespace studio {

void Popup::drawWindow(turbine::Context &ctx, bool *open, std::function<void()> const&drawContents) {
	studio::ig::centerNextWindow(ctx);
	ImGui::SetNextWindowSize(static_cast<ImVec2>(m_size));
	constexpr auto modalFlags = ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize;
	if (ImGui::BeginPopupModal(m_title.c_str(), open, modalFlags)) {
		drawContents();
		ImGui::EndPopup();
	}
}

}