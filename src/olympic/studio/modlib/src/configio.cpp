/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <studio/configio.hpp>

namespace studio {

constexpr auto ConfigDir = [] {
	switch (ox::defines::OS) {
		case ox::OS::Darwin:
			return "{}/Library/Preferences/{}";
		case ox::OS::DragonFlyBSD:
		case ox::OS::FreeBSD:
		case ox::OS::Linux:
		case ox::OS::NetBSD:
		case ox::OS::OpenBSD:
			return "{}/.config/{}";
		case ox::OS::Windows:
			return R"({}/AppData/Local/{})";
		case ox::OS::BareMetal:
			return "";
	}
}();

ox::String configPath(const keel::Context &ctx) noexcept {
	const auto homeDir = std::getenv(ox::defines::OS == ox::OS::Windows ? "USERPROFILE" : "HOME");
	return ox::sfmt(ConfigDir, homeDir, ctx.appName);
}

}
