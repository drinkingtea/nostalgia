/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <ox/std/string.hpp>

#include <studio/module.hpp>

namespace studio {

void registerModule(const studio::Module*) noexcept;

}
