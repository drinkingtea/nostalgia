/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <ox/claw/claw.hpp>
#include <ox/event/signal.hpp>
#include <ox/std/string.hpp>

#include <studio/itemmaker.hpp>
#include <studio/popup.hpp>

namespace studio {

class NewMenu: public studio::Popup {
	public:
		enum class Stage {
			Closed,
			Opening,
			NewItemType,
			NewItemName,
		};

		// emits path parameter
		ox::Signal<ox::Error(ox::StringView)> finished;

	private:
		Stage m_stage = Stage::Closed;
		ox::String m_typeName;
		ox::BString<255> m_itemName;
		ox::Vector<ox::UniquePtr<studio::ItemMaker>> m_types;
		int m_selectedType = 0;
		bool m_open = false;

	public:
		NewMenu() noexcept;

		void open() noexcept override;

		void close() noexcept override;

		[[nodiscard]]
		bool isOpen() const noexcept override;

		void draw(turbine::Context &ctx) noexcept override;

		template<typename T>
		void addItemType(ox::String name, ox::String parentDir, ox::String fileExt, T itemTempl, ox::ClawFormat pFmt = ox::ClawFormat::Metal) noexcept;

		template<typename T>
		void addItemType(ox::String name, ox::String parentDir, ox::String fileExt, ox::ClawFormat pFmt = ox::ClawFormat::Metal) noexcept;

		void addItemMaker(ox::UniquePtr<studio::ItemMaker> im) noexcept;

	private:
		void drawNewItemType(turbine::Context &ctx) noexcept;

		void drawNewItemName(turbine::Context &ctx) noexcept;

		void drawFirstPageButtons() noexcept;

		void drawLastPageButtons(turbine::Context &ctx) noexcept;

		void finish(turbine::Context &ctx) noexcept;

};

template<typename T>
void NewMenu::addItemType(ox::String displayName, ox::String parentDir, ox::String fileExt, T itemTempl, ox::ClawFormat pFmt) noexcept {
	m_types.emplace_back(ox::make<studio::ItemMakerT<T>>(std::move(displayName), std::move(parentDir), std::move(fileExt), std::move(itemTempl), pFmt));
}

template<typename T>
void NewMenu::addItemType(ox::String displayName, ox::String parentDir, ox::String fileExt, ox::ClawFormat pFmt) noexcept {
	m_types.emplace_back(ox::make<studio::ItemMakerT<T>>(std::move(displayName), std::move(parentDir), std::move(fileExt), pFmt));
}

}
