/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <ox/event/signal.hpp>
#include <ox/std/string.hpp>

#include <turbine/context.hpp>

#include <studio/popup.hpp>

namespace studio {

class AboutPopup: public studio::Popup {
	public:
		enum class Stage {
			Closed,
			Opening,
			Open,
		};

	private:
		Stage m_stage = Stage::Closed;
		ox::String m_text;

	public:
		explicit AboutPopup(turbine::Context &ctx) noexcept;

		void open() noexcept override;

		void close() noexcept override;

		[[nodiscard]]
		bool isOpen() const noexcept override;

		void draw(turbine::Context &ctx) noexcept override;

};

}
