/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <ox/model/modelvalue.hpp>
#include <turbine/context.hpp>

#include <studio/editor.hpp>

namespace studio {

class ClawEditor: public studio::Editor {
	private:
		using ObjPath = ox::Vector<ox::StringView, 8>;
		ox::ModelObject m_obj;
	public:
		ClawEditor(ox::CRStringView path, ox::ModelObject obj) noexcept;

		void draw(turbine::Context&) noexcept final;

	private:
		static void drawRow(const ox::ModelValue &value) noexcept;

		void drawVar(ObjPath *path, ox::CRStringView name, const ox::ModelValue &value) noexcept;

		void drawTree(ObjPath *path, const ox::ModelObject &obj) noexcept;
};

}
