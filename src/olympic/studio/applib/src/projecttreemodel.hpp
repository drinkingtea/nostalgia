/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <ox/std/memory.hpp>
#include <ox/std/string.hpp>

#include <turbine/context.hpp>

namespace studio {

class ProjectTreeModel {
	private:
		class ProjectExplorer *m_explorer = nullptr;
		ProjectTreeModel *m_parent = nullptr;
		ox::String m_name;
		ox::Vector<ox::UniquePtr<ProjectTreeModel>> m_children;
	public:
		explicit ProjectTreeModel(class ProjectExplorer *explorer, ox::String name,
		                          ProjectTreeModel *parent = nullptr) noexcept;

		ProjectTreeModel(ProjectTreeModel &&other) noexcept;

		void draw(turbine::Context &ctx) const noexcept;

		void setChildren(ox::Vector<ox::UniquePtr<ProjectTreeModel>> children) noexcept;

	private:
		[[nodiscard]]
		ox::BasicString<255> fullPath() const noexcept;
};

}
