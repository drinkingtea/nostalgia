/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <turbine/gfx.hpp>

#include "filedialogmanager.hpp"

namespace studio {

studio::TaskState FileDialogManager::update(turbine::Context &ctx) noexcept {
	switch (m_state) {
		case UpdateProjectPathState::EnableSystemCursor: {
			// switch to system cursor in this update and open file dialog in the next
			// the cursor state has to be set before the ImGui frame starts
			m_state = UpdateProjectPathState::RunFileDialog;
			break;
		}
		case UpdateProjectPathState::RunFileDialog: {
			// switch to system cursor
			auto [path, err] = studio::chooseDirectory();
			// Mac file dialog doesn't restore focus to main window when closed...
			turbine::focusWindow(ctx);
			if (!err) {
				err = pathChosen.emitCheckError(path);
				oxAssert(err, "Path chosen response failed");
			}
			m_state = UpdateProjectPathState::None;
			return studio::TaskState::Done;
		}
		case UpdateProjectPathState::None:
			break;
	}
	return studio::TaskState::Running;
}

}
