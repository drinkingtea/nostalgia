/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <ox/event/signal.hpp>
#include <ox/std/memory.hpp>

#include <studio/widget.hpp>
#include "projecttreemodel.hpp"

namespace studio {

class ProjectExplorer: public studio::Widget {
	private:
		ox::UniquePtr<ProjectTreeModel> m_treeModel;
		turbine::Context &m_ctx;
	public:
		explicit ProjectExplorer(turbine::Context &ctx) noexcept;

		void draw(turbine::Context &ctx) noexcept override;

		void setModel(ox::UniquePtr<ProjectTreeModel> model) noexcept;

		ox::Error refreshProjectTreeModel(ox::CRStringView = {}) noexcept;

		[[nodiscard]]
		inline ox::FileSystem *romFs() noexcept {
			return rom(m_ctx);
		}

	// slots
	public:
		ox::Signal<ox::Error(const ox::StringView&)> fileChosen;
};

}
