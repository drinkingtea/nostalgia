/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <imgui.h>

#include <keel/media.hpp>
#include <glutils/glutils.hpp>
#include <turbine/turbine.hpp>

#include <studio/configio.hpp>
#include "clawviewer.hpp"
#include "filedialogmanager.hpp"
#include "studioapp.hpp"

namespace studio {

ox::Vector<const studio::Module*> modules;

struct StudioConfig {
	static constexpr auto TypeName = "net.drinkingtea.studio.StudioConfig";
	static constexpr auto TypeVersion = 1;
	ox::String projectPath;
	ox::String activeTabItemName;
	ox::Vector<ox::String> openFiles;
	bool showProjectExplorer = true;
};

oxModelBegin(StudioConfig)
	oxModelFieldRename(active_tab_item_name, activeTabItemName)
	oxModelFieldRename(project_path, projectPath)
	oxModelFieldRename(open_files, openFiles)
	oxModelFieldRename(show_project_explorer, showProjectExplorer)
oxModelEnd()

StudioUI::StudioUI(turbine::Context *ctx, ox::StringView projectDir) noexcept:
	m_ctx(*ctx),
	m_projectDir(projectDir),
	m_projectExplorer(ox::make_unique<ProjectExplorer>(m_ctx)),
	m_aboutPopup(*ctx) {
	m_projectExplorer->fileChosen.connect(this, &StudioUI::openFile);
	ImGui::GetIO().IniFilename = nullptr;
	loadModules();
	// open project and files
	const auto [config, err] = studio::readConfig<StudioConfig>(keelCtx(*ctx));
	m_showProjectExplorer = config.showProjectExplorer;
	if (!err) {
		oxIgnoreError(openProject(config.projectPath));
		for (const auto &f : config.openFiles) {
			auto openFileErr = openFileActiveTab(f, config.activeTabItemName == f);
			if (openFileErr) {
				oxErrorf("\nCould not open editor for file:\n\t{}\nReason:\n\t{}\n", f, toStr(openFileErr));
			}
		}
	} else {
		if constexpr(!ox::defines::Debug) {
			oxErrf("Could not open studio config file: {}: {}\n", err.errCode, toStr(err));
		} else {
			oxErrf(
					"Could not open studio config file: {}: {} ({}:{})\n",
					err.errCode, toStr(err), err.file, err.line);
		}
	}
}

void StudioUI::update() noexcept {
	m_taskRunner.update(m_ctx);
}

void StudioUI::handleKeyEvent(turbine::Key key, bool down) noexcept {
	const auto ctrlDown = turbine::buttonDown(m_ctx, turbine::Key::Mod_Ctrl);
	for (auto p : m_popups) {
		if (p->isOpen()) {
			if (key == turbine::Key::Escape) {
				p->close();
			}
			return;
		}
	}
	if (down && ctrlDown) {
		switch (key) {
			case turbine::Key::Num_1:
				toggleProjectExplorer();
				break;
			case turbine::Key::Alpha_C:
				m_activeEditor->copy();
				break;
			case turbine::Key::Alpha_N:
				m_newMenu.open();
				break;
			case turbine::Key::Alpha_O:
				m_taskRunner.add(*ox::make<FileDialogManager>(this, &StudioUI::openProject));
				break;
			case turbine::Key::Alpha_Q:
				turbine::requestShutdown(m_ctx);
				break;
			case turbine::Key::Alpha_S:
				save();
				break;
			case turbine::Key::Alpha_V:
				m_activeEditor->paste();
				break;
			case turbine::Key::Alpha_X:
				m_activeEditor->cut();
				break;
			case turbine::Key::Alpha_Y:
				redo();
				break;
			case turbine::Key::Alpha_Z:
				undo();
				break;
			default:
				if (m_activeEditor) {
					m_activeEditor->keyStateChanged(key, down);
				}
				break;
		}
	} else if (m_activeEditor && !ctrlDown) {
		m_activeEditor->keyStateChanged(key, down);
	}
}

void StudioUI::draw() noexcept {
	glutils::clearScreen();
	drawMenu();
	const auto viewport = ImGui::GetMainViewport();
	constexpr auto menuHeight = 18;
	ImGui::SetNextWindowPos(ImVec2(viewport->Pos.x, viewport->Pos.y + menuHeight));
	ImGui::SetNextWindowSize(ImVec2(viewport->Size.x, viewport->Size.y - menuHeight));
	constexpr auto windowFlags = ImGuiWindowFlags_NoTitleBar
	                           | ImGuiWindowFlags_NoResize
	                           | ImGuiWindowFlags_NoMove
	                           | ImGuiWindowFlags_NoScrollbar
	                           | ImGuiWindowFlags_NoSavedSettings;
	ImGui::Begin("MainWindow##Studio", nullptr, windowFlags);
	{
		if (m_showProjectExplorer) {
			m_projectExplorer->draw(m_ctx);
			ImGui::SameLine();
		}
		drawTabBar();
		for (auto &w: m_widgets) {
			w->draw(m_ctx);
		}
		for (auto p: m_popups) {
			p->draw(m_ctx);
		}
	}
	ImGui::End();
}

void StudioUI::drawMenu() noexcept {
	if (ImGui::BeginMainMenuBar()) {
		if (ImGui::BeginMenu("File")) {
			if (ImGui::MenuItem("New...", "Ctrl+N")) {
				m_newMenu.open();
			}
			if (ImGui::MenuItem("Open Project...", "Ctrl+O")) {
				m_taskRunner.add(*ox::make<FileDialogManager>(this, &StudioUI::openProject));
			}
			if (ImGui::MenuItem("Save", "Ctrl+S", false, m_activeEditor && m_activeEditor->unsavedChanges())) {
				m_activeEditor->save();
			}
			if (ImGui::MenuItem("Quit", "Ctrl+Q")) {
				turbine::requestShutdown(m_ctx);
			}
			ImGui::EndMenu();
		}
		if (ImGui::BeginMenu("Edit")) {
			auto undoStack = m_activeEditor ? m_activeEditor->undoStack() : nullptr;
			if (ImGui::MenuItem("Undo", "Ctrl+Z", false, undoStack && undoStack->canUndo())) {
				 m_activeEditor->undoStack()->undo();
			}
			if (ImGui::MenuItem("Redo", "Ctrl+Y", false, undoStack && undoStack->canRedo())) {
				 m_activeEditor->undoStack()->redo();
			}
			ImGui::Separator();
			if (ImGui::MenuItem("Copy", "Ctrl+C")) {
				m_activeEditor->copy();
			}
			if (ImGui::MenuItem("Cut", "Ctrl+X")) {
				m_activeEditor->cut();
			}
			if (ImGui::MenuItem("Paste", "Ctrl+V")) {
				m_activeEditor->paste();
			}
			ImGui::EndMenu();
		}
		if (ImGui::BeginMenu("View")) {
			if (ImGui::MenuItem("Project Explorer", "Ctrl+1", m_showProjectExplorer)) {
				toggleProjectExplorer();
			}
			ImGui::EndMenu();
		}
		if (ImGui::BeginMenu("Help")) {
			if (ImGui::MenuItem("About")) {
				m_aboutPopup.open();
			}
			ImGui::EndMenu();
		}
		ImGui::EndMainMenuBar();
	}
}

void StudioUI::drawTabBar() noexcept {
	const auto viewport = ImGui::GetContentRegionAvail();
	ImGui::BeginChild("TabWindow##MainWindow##Studio", ImVec2(viewport.x, viewport.y));
	constexpr auto tabBarFlags = ImGuiTabBarFlags_Reorderable | ImGuiTabBarFlags_TabListPopupButton;
	if (ImGui::BeginTabBar("TabBar##TabWindow##MainWindow##Studio", tabBarFlags)) {
		drawTabs();
		ImGui::EndTabBar();
	}
	ImGui::EndChild();
}

void StudioUI::drawTabs() noexcept {
	for (auto it = m_editors.begin(); it != m_editors.end();) {
		auto const &e = *it;
		auto open = true;
		const auto unsavedChanges = e->unsavedChanges() ? ImGuiTabItemFlags_UnsavedDocument : 0;
		const auto selected = m_activeEditorUpdatePending == e.get() ?  ImGuiTabItemFlags_SetSelected : 0;
		const auto flags = unsavedChanges | selected;
		if (ImGui::BeginTabItem(e->itemDisplayName().c_str(), &open, flags)) {
			if (m_activeEditor != e.get()) {
				m_activeEditor = e.get();
				studio::editConfig<StudioConfig>(keelCtx(m_ctx), [&](StudioConfig *config) {
					config->activeTabItemName = m_activeEditor->itemPath();
				});
			}
			if (m_activeEditorUpdatePending == e.get()) {
				m_activeEditorUpdatePending = nullptr;
			}
			if (m_activeEditorOnLastDraw != e.get()) [[unlikely]] {
				m_activeEditor->onActivated();
				turbine::setConstantRefresh(m_ctx, m_activeEditor->requiresConstantRefresh());
			}
			e->draw(m_ctx);
			m_activeEditorOnLastDraw = e.get();
			ImGui::EndTabItem();
		}
		if (!open) {
			e->close();
			try {
				oxThrowError(m_editors.erase(it).moveTo(&it));
			} catch (const ox::Exception &ex) {
				oxErrf("Editor tab deletion failed: {} ({}:{})\n", ex.what(), ex.file, ex.line);
			} catch (const std::exception &ex) {
				oxErrf("Editor tab deletion failed: {}\n", ex.what());
			}
		} else {
			++it;
		}
	}
}

void StudioUI::loadEditorMaker(studio::EditorMaker const&editorMaker) noexcept {
	for (auto const&ext : editorMaker.fileTypes) {
		m_editorMakers[ext] = editorMaker.make;
	}
}

void StudioUI::loadModule(const studio::Module *mod) noexcept {
	for (const auto &editorMaker : mod->editors(m_ctx)) {
		loadEditorMaker(editorMaker);
	}
	for (auto &im : mod->itemMakers(m_ctx)) {
		m_newMenu.addItemMaker(std::move(im));
	}
}

void StudioUI::loadModules() noexcept {
	for (auto &mod : modules) {
		loadModule(mod);
	}
}

void StudioUI::toggleProjectExplorer() noexcept {
	m_showProjectExplorer = !m_showProjectExplorer;
	studio::editConfig<StudioConfig>(keelCtx(m_ctx), [&](StudioConfig *config) {
		config->showProjectExplorer = m_showProjectExplorer;
	});
}

void StudioUI::redo() noexcept {
	auto undoStack = m_activeEditor ? m_activeEditor->undoStack() : nullptr;
	if (undoStack && undoStack->canRedo()) {
		 m_activeEditor->undoStack()->redo();
	}
}

void StudioUI::undo() noexcept {
	auto undoStack = m_activeEditor ? m_activeEditor->undoStack() : nullptr;
	if (undoStack && undoStack->canUndo()) {
		 m_activeEditor->undoStack()->undo();
	}
}

void StudioUI::save() noexcept {
	if (m_activeEditor && m_activeEditor->unsavedChanges()) {
		m_activeEditor->save();
	}
}

ox::Error StudioUI::openProject(ox::CRStringView path) noexcept {
	oxRequireM(fs, keel::loadRomFs(path));
	oxReturnError(keel::setRomFs(keelCtx(m_ctx), std::move(fs)));
	turbine::setWindowTitle(m_ctx, ox::sfmt("{} - {}", keelCtx(m_ctx).appName, path));
	m_project = ox::make_unique<studio::Project>(keelCtx(m_ctx), ox::String(path), m_projectDir);
	auto sctx = applicationData<studio::StudioContext>(m_ctx);
	sctx->project = m_project.get();
	m_project->fileAdded.connect(m_projectExplorer.get(), &ProjectExplorer::refreshProjectTreeModel);
	m_project->fileDeleted.connect(m_projectExplorer.get(), &ProjectExplorer::refreshProjectTreeModel);
	m_openFiles.clear();
	m_editors.clear();
	studio::editConfig<StudioConfig>(keelCtx(m_ctx), [&](StudioConfig *config) {
		config->projectPath = ox::String(path);
		config->openFiles.clear();
	});
	return m_projectExplorer->refreshProjectTreeModel();
}

ox::Error StudioUI::openFile(ox::CRStringView path) noexcept {
	return openFileActiveTab(path, true);
}

ox::Error StudioUI::openFileActiveTab(ox::CRStringView path, bool makeActiveTab) noexcept {
	if (m_openFiles.contains(path)) {
		for (auto &e : m_editors) {
			if (makeActiveTab && e->itemPath() == path) {
				m_activeEditor = e.get();
				m_activeEditorUpdatePending = e.get();
				break;
			}
		}
		return OxError(0);
	}
	oxRequire(ext, studio::fileExt(path).to<ox::String>([](auto const&v) {return ox::String(v);}));
	// create Editor
	studio::BaseEditor *editor = nullptr;
	if (!m_editorMakers.contains(ext)) {
		auto [obj, err] = m_project->loadObj<ox::ModelObject>(path);
		if (err) {
			return OxError(1, "There is no editor for this file extension");
		}
		editor = ox::make<ClawEditor>(path, std::move(obj));
	} else {
		const auto err = m_editorMakers[ext](path).moveTo(&editor);
		if (err) {
			if constexpr(!ox::defines::Debug) {
				oxErrf("Could not open Editor: {}\n", toStr(err));
			} else {
				oxErrf("Could not open Editor: {} ({}:{})\n", err.errCode, err.file, err.line);
			}
			return err;
		}
	}
	editor->closed.connect(this, &StudioUI::closeFile);
	m_editors.emplace_back(editor);
	m_openFiles.emplace_back(path);
	if (makeActiveTab) {
		m_activeEditor = m_editors.back().value->get();
		m_activeEditorUpdatePending = editor;
	}
	// save to config
	studio::editConfig<StudioConfig>(keelCtx(m_ctx), [&](StudioConfig *config) {
		if (!config->openFiles.contains(path)) {
			config->openFiles.emplace_back(path);
		}
	});
	return {};
}

ox::Error StudioUI::closeFile(ox::CRStringView path) noexcept {
	if (!m_openFiles.contains(path)) {
		return OxError(0);
	}
	oxIgnoreError(m_openFiles.erase(std::remove(m_openFiles.begin(), m_openFiles.end(), path)));
	// save to config
	studio::editConfig<StudioConfig>(keelCtx(m_ctx), [&](StudioConfig *config) {
		oxIgnoreError(config->openFiles.erase(std::remove(config->openFiles.begin(), config->openFiles.end(), path)));
	});
	return OxError(0);
}

void registerModule(const studio::Module *mod) noexcept {
	modules.emplace_back(mod);
}

}
