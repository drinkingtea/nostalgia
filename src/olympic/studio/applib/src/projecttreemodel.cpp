/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <imgui.h>

#include "projectexplorer.hpp"
#include "projecttreemodel.hpp"

namespace studio {

ProjectTreeModel::ProjectTreeModel(ProjectExplorer *explorer, ox::String name,
                                   ProjectTreeModel *parent) noexcept:
	m_explorer(explorer),
	m_parent(parent),
	m_name(std::move(name)) {
}

ProjectTreeModel::ProjectTreeModel(ProjectTreeModel &&other) noexcept:
	m_explorer(other.m_explorer),
	m_parent(other.m_parent),
	m_name(std::move(other.m_name)),
	m_children(std::move(other.m_children)) {
}

void ProjectTreeModel::draw(turbine::Context &ctx) const noexcept {
	constexpr auto dirFlags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick;
	if (!m_children.empty()) {
		if (ImGui::TreeNodeEx(m_name.c_str(), dirFlags)) {
			for (const auto &child : m_children) {
				child->draw(ctx);
			}
			ImGui::TreePop();
		}
	} else {
		const auto path = fullPath();
		const auto name = ox::sfmt<ox::BasicString<255>>("{}##{}", m_name, path);
		if (ImGui::TreeNodeEx(name.c_str(), ImGuiTreeNodeFlags_Leaf)) {
			if (ImGui::IsItemHovered() && ImGui::IsMouseDoubleClicked(0)) {
				m_explorer->fileChosen.emit(path);
			}
			ImGui::TreePop();
		}
	}
}

void ProjectTreeModel::setChildren(ox::Vector<ox::UniquePtr<ProjectTreeModel>> children) noexcept {
	m_children = std::move(children);
}

ox::BasicString<255> ProjectTreeModel::fullPath() const noexcept {
	if (m_parent) {
		return m_parent->fullPath() + "/" + ox::StringView(m_name);
	}
	return {};
}

}
