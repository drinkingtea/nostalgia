/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <imgui.h>

#include <studio/imguiuitl.hpp>
#include "aboutpopup.hpp"

namespace studio {

AboutPopup::AboutPopup(turbine::Context &ctx) noexcept {
	m_text = ox::sfmt("{} - dev build", keelCtx(ctx).appName);
}

void AboutPopup::open() noexcept {
	m_stage = Stage::Opening;
}

void AboutPopup::close() noexcept {
	m_stage = Stage::Closed;
}

bool AboutPopup::isOpen() const noexcept {
	return m_stage == Stage::Open;
}

void AboutPopup::draw(turbine::Context &ctx) noexcept {
	switch (m_stage) {
		case Stage::Closed:
			break;
		case Stage::Opening:
			ImGui::OpenPopup("About");
			m_stage = Stage::Open;
			[[fallthrough]];
		case Stage::Open: {
			constexpr auto modalFlags =
					ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize;
			ImGui::SetNextWindowSize(ImVec2(215, 90));
			studio::ig::centerNextWindow(ctx);
			auto open = true;
			if (ImGui::BeginPopupModal("About", &open, modalFlags)) {
				ImGui::Text("%s", m_text.c_str());
				ImGui::NewLine();
				ImGui::Dummy(ImVec2(148.0f, 0.0f));
				ImGui::SameLine();
				if (ImGui::Button("Close")) {
					ImGui::CloseCurrentPopup();
					open = false;
				}
				ImGui::EndPopup();
			}
			if (!open) {
				m_stage = Stage::Closed;
			}
			break;
		}
	}
}

}
