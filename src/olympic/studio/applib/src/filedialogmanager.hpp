/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <ox/event/signal.hpp>
#include <ox/std/string.hpp>

#include <turbine/context.hpp>

#include <studio/filedialog.hpp>
#include <studio/task.hpp>

namespace studio {

class FileDialogManager : public studio::Task {
	private:
		enum class UpdateProjectPathState {
				None,
				EnableSystemCursor,
				RunFileDialog,
				Start = EnableSystemCursor,
		} m_state = UpdateProjectPathState::Start;

	public:
		FileDialogManager() noexcept = default;

		template<class... Args>
		explicit FileDialogManager(Args ...args) noexcept {
			pathChosen.connect(args...);
		}

		~FileDialogManager() noexcept override = default;

		studio::TaskState update(turbine::Context &ctx) noexcept final;

		// signals
		ox::Signal<ox::Error(const ox::String &)> pathChosen;

};

}
