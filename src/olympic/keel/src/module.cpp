/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <keel/module.hpp>

namespace keel {

static ox::Vector<const Module*> mods;

void registerModule(const Module *mod) noexcept {
	mods.emplace_back(mod);
}

[[nodiscard]]
ox::Vector<const Module*> const&modules() noexcept {
	return mods;
}


ox::Vector<TypeDescGenerator> Module::types() const noexcept {
	return {};
}

ox::Vector<const keel::BaseConverter*> Module::converters() const noexcept {
	return {};
}

ox::Vector<PackTransform> Module::packTransforms() const noexcept {
	return {};
}

}
