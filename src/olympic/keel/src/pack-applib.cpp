/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <fstream>

#include <ox/clargs/clargs.hpp>
#include <ox/fs/fs.hpp>
#include <ox/logconn/def.hpp>
#include <ox/logconn/logconn.hpp>

#include <keel/keel.hpp>

static ox::Error writeFileBuff(ox::StringView path, ox::Buffer const&buff) noexcept {
	try {
		std::ofstream f(std::string(toStdStringView(path)), std::ios::binary);
		f.write(buff.data(), static_cast<intptr_t>(buff.size()));
	} catch (std::fstream::failure const&) {
		return OxError(2, "failed to write file");
	}
	return {};
}

static ox::Result<ox::Buffer> readFileBuff(ox::StringView path) noexcept {
	std::ifstream file(std::string(toStdStringView(path)), std::ios::binary | std::ios::ate);
	if (!file.good()) {
		oxErrorf("Could not find OxFS file: {}", path);
		return OxError(1, "Could not find OxFS file");
	}
	try {
		const auto size = static_cast<std::size_t>(file.tellg());
		ox::Buffer buff(size);
		file.seekg(0, std::ios::beg);
		file.read(buff.data(), static_cast<std::streamsize>(buff.size()));
		return buff;
	} catch (std::ios_base::failure const&e) {
		oxErrorf("Could not read OxFS file: {}", e.what());
		return OxError(2, "Could not read OxFS file");
	}
}

static ox::Error generateTypes(ox::TypeStore *ts) noexcept {
	for (const auto mod : keel::modules()) {
		for (auto gen : mod->types()) {
			oxReturnError(gen(*ts));
		}
	}
	return {};
}

static ox::Error pack(ox::StringView argSrc, ox::StringView argRomBin, ox::StringView projectDataDir) noexcept {
	ox::Buffer dstBuff(32 * ox::units::MB);
	oxReturnError(ox::FileSystem32::format(dstBuff.data(), dstBuff.size()));
	ox::FileSystem32 dst(dstBuff);
	oxRequire(ctx, keel::init(ox::make_unique<ox::PassThroughFS>(argSrc), "keel-pack"));
	keel::TypeStore ts(*ctx->rom, ox::sfmt("{}/type_descriptors", projectDataDir));
	oxReturnError(generateTypes(&ts));
	oxReturnError(keel::pack(*ctx, ts, dst));
	oxRequireM(pl, keel::GbaPreloader::make());
	oxReturnError(preload(ts, dst, *pl));
	oxReturnError(dst.resize());
	// resize buffer
	oxRequire(dstSize, dst.size());
	dstBuff.resize(dstSize);

	oxRequireM(romBuff, readFileBuff(argRomBin));
	oxReturnError(appendBinary(romBuff, dstBuff, *pl));

	oxOutf("Dest FS size: {} bytes\n", dstSize);
	oxOutf("Preload buff size: {} bytes\n", pl->buff().size());
	oxOutf("ROM buff size: {} bytes\n", romBuff.size());

	oxReturnError(writeFileBuff(argRomBin, romBuff));
	return {};
}

static ox::Error run(int argc, const char **argv, ox::StringView projectDataDir) noexcept {
	ox::ClArgs const args(argc, argv);
	const auto argSrc = args.getString("src", "");
	const auto argRomBin = args.getString("rom-bin", "");
	if (argSrc == "") {
		oxErr("\033[31;1;1merror:\033[0m must specify a source directory\n");
		return OxError(1, "must specify a source directory");
	}
	if (argRomBin == "") {
		oxErr("\033[31;1;1merror:\033[0m must specify a path for ROM file\n");
		return OxError(1, "must specify a path for preload file");
	}
	return pack(argSrc, argRomBin, projectDataDir);
}

namespace olympic {

ox::Error run(
		[[maybe_unused]] ox::StringView project,
		[[maybe_unused]] ox::StringView appName,
		ox::StringView projectDataDir,
		int argc,
		const char **argv) noexcept {
	return ::run(argc, argv, projectDataDir);
}

}
