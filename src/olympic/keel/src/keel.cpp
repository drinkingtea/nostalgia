/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <keel/keel.hpp>

namespace keel {

ox::Error init(
		keel::Context &ctx,
		ox::UPtr<ox::FileSystem> &&fs,
		ox::CRStringView appName) noexcept {
	ctx.appName = appName;
	oxIgnoreError(setRomFs(ctx, std::move(fs)));
#ifndef OX_BARE_METAL
	const auto &mods = modules();
	for (auto &mod : mods) {
		// register type converters
		for (auto c : mod->converters()) {
			ctx.converters.emplace_back(c);
		}
		// register pack transforms
		for (auto c : mod->packTransforms()) {
			ctx.packTransforms.emplace_back(c);
		}
	}
#endif
	return {};
}

ox::Result<ox::UPtr<Context>> init(ox::UPtr<ox::FileSystem> &&fs, ox::CRStringView appName) noexcept {
	auto ctx = ox::make_unique<Context>();
	oxReturnError(keel::init(*ctx, std::move(fs), appName));
	return ctx;
}

}
