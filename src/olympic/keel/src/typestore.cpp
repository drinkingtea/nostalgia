/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <keel/typestore.hpp>

namespace keel {

TypeStore::TypeStore(ox::FileSystem &fs, ox::StringView descPath) noexcept:
	m_fs(fs),
	m_descPath(std::move(descPath)) {
}

ox::Result<ox::UniquePtr<ox::DescriptorType>> TypeStore::loadDescriptor(ox::CRStringView typeId) noexcept {
	auto path = ox::sfmt("{}/{}", m_descPath, typeId);
	oxRequire(buff, m_fs.read(path));
	auto dt = ox::make_unique<ox::DescriptorType>();
	oxReturnError(ox::readClaw<ox::DescriptorType>(buff, dt.get()));
	return dt;
}

}
