/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#undef NDEBUG

#include <map>
#include <ox/std/std.hpp>

#include <keel/keel.hpp>

static std::map<ox::StringView, ox::Error(*)()> tests = {
	{
		"writeUuidHeader",
		[]() -> ox::Error {
			constexpr ox::StringView uuidStr = "8d814442-f46e-4cc3-8edc-ca3c01cc86db";
			constexpr ox::StringView hdr = "K1;8d814442-f46e-4cc3-8edc-ca3c01cc86db;";
			oxRequire(uuid, ox::UUID::fromString(uuidStr));
			ox::Array<char, hdr.len()> buff;
			ox::CharBuffWriter bw(buff);
			oxReturnError(keel::writeUuidHeader(bw, uuid));
			oxExpect(ox::StringView(buff.data(), buff.size()), hdr);
			return {};
		}
	},
};

int main(int argc, const char **args) {
	int retval = -1;
	if (argc > 0) {
		auto testName = args[1];
		if (tests.find(testName) != tests.end()) {
			retval = static_cast<int>(tests[testName]());
		} else {
			retval = 1;
		}
	}
	return retval;
}
