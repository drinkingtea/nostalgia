/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <ox/claw/claw.hpp>
#include <ox/fs/fs.hpp>

namespace keel {

constexpr auto K1HdrSz = 40;

ox::Result<ox::UUID> readUuidHeader(const ox::Buffer &buff) noexcept;

ox::Result<ox::UUID> readUuidHeader(const char *buff, std::size_t buffLen) noexcept;

ox::Error writeUuidHeader(ox::Writer_c auto &writer, const ox::UUID &uuid) noexcept {
	oxReturnError(write(writer, "K1;"));
	oxReturnError(uuid.toString(writer));
	return writer.put(';');
}

template<typename T>
ox::Result<T> readAsset(const ox::Buffer &buff) noexcept {
	std::size_t offset = 0;
	const auto err = readUuidHeader(buff).error;
	if (!err) {
		offset = K1HdrSz; // the size of K1 headers
	}
	return ox::readClaw<T>(buff.data() + offset, buff.size() - offset);
}

ox::Result<ox::ModelObject> readAsset(ox::TypeStore *ts, const ox::Buffer &buff) noexcept;

struct AssetHdr {
	ox::UUID uuid;
	ox::ClawHeader clawHdr;
};

ox::Result<AssetHdr> readAssetHeader(const char *buff, std::size_t buffLen) noexcept;

ox::Result<AssetHdr> readAssetHeader(const ox::Buffer &buff) noexcept;

}
