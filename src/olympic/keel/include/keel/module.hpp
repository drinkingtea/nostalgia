/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <ox/std/vector.hpp>
#include <ox/model/descwrite.hpp>

#include "typeconv.hpp"

namespace keel {

using TypeDescGenerator = ox::Error(*)(ox::TypeStore&);

template<typename T>
ox::Error generateTypeDesc(ox::TypeStore &ts) noexcept {
	return ox::buildTypeDef<T>(&ts).error;
}

class Module {
	public:
		constexpr Module() noexcept = default;
		Module(Module const&) noexcept = delete;
		Module(Module&&) noexcept = delete;
		Module &operator=(Module const&) noexcept = delete;
		Module &operator=(Module&&) noexcept = delete;
		constexpr virtual ~Module() noexcept = default;

		[[nodiscard]]
		virtual ox::String id() const noexcept = 0;
		[[nodiscard]]
		virtual ox::Vector<TypeDescGenerator> types() const noexcept;
		[[nodiscard]]
		virtual ox::Vector<const keel::BaseConverter*> converters() const noexcept;
		[[nodiscard]]
		virtual ox::Vector<PackTransform> packTransforms() const noexcept;
};

void registerModule(const Module *mod) noexcept;

[[nodiscard]]
ox::Vector<const keel::Module*> const&modules() noexcept;

}
