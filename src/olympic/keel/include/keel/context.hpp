/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <ox/fs/fs.hpp>
#include <ox/std/memory.hpp>

#include "assetmanager.hpp"

namespace keel {

class Context;
using PackTransform = ox::Error(*)(Context&, ox::Buffer &clawData);

class Context {
	public:
		ox::UPtr<ox::FileSystem> rom;
		ox::BasicString<32> appName{"Keel App"};
#ifndef OX_BARE_METAL
		AssetManager assetManager;
		ox::HashMap<ox::String, ox::UUID> pathToUuid;
		ox::HashMap<ox::UUIDStr, ox::String> uuidToPath;
		ox::Vector<const class BaseConverter*> converters;
		ox::Vector<PackTransform> packTransforms;
#else
		std::size_t preloadSectionOffset = 0;
#endif

		constexpr Context() noexcept = default;
		Context(Context const&) noexcept = delete;
		Context(Context&&) noexcept = delete;
		Context &operator=(Context const&) noexcept = delete;
		Context &operator=(Context&&) noexcept = delete;
		constexpr virtual ~Context() noexcept = default;
};

}
