/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <ox/std/memory.hpp>

#include "context.hpp"
#include "media.hpp"
#include "module.hpp"
#include "pack.hpp"
#include "typestore.hpp"

namespace keel {

ox::Error init(
		keel::Context &ctx,
		ox::UPtr<ox::FileSystem> &&fs,
		ox::CRStringView appName) noexcept;

ox::Result<ox::UPtr<Context>> init(
		ox::UPtr<ox::FileSystem> &&fs,
		ox::CRStringView appName) noexcept;

}
