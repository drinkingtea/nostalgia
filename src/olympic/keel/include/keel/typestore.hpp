/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

#include <ox/claw/claw.hpp>
#include <ox/fs/fs.hpp>
#include <ox/model/typestore.hpp>

namespace keel {

class TypeStore: public ox::TypeStore {
	private:
		ox::FileSystem &m_fs;
		ox::String m_descPath;

	public:
		explicit TypeStore(ox::FileSystem &fs, ox::StringView descPath) noexcept;

	protected:
		ox::Result<ox::UniquePtr<ox::DescriptorType>> loadDescriptor(ox::CRStringView typeId) noexcept override;
};

}
