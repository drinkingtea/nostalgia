/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include <ox/logconn/def.hpp>
#include <ox/logconn/logconn.hpp>

#ifndef OLYMPIC_PROJECT_NAME
#define OLYMPIC_PROJECT_NAME "OlympicProject"
#endif

#ifndef OLYMPIC_APP_NAME
#define OLYMPIC_APP_NAME "App"
#endif

#ifndef OLYMPIC_PROJECT_NAMESPACE
#define OLYMPIC_PROJECT_NAMESPACE project
#endif

#ifndef OLYMPIC_PROJECT_DATADIR
#define OLYMPIC_PROJECT_DATADIR ".keel"
#endif

#ifndef OLYMPIC_LOAD_KEEL_MODULES
#define OLYMPIC_LOAD_KEEL_MODULES 1
#endif

#ifndef OLYMPIC_LOAD_STUDIO_MODULES
#define OLYMPIC_LOAD_STUDIO_MODULES 0
#endif

namespace olympic {

ox::Error run(
		ox::StringView project,
		ox::StringView appName,
		ox::StringView projectDataDir,
		int argc,
		const char **argv) noexcept;

}

namespace OLYMPIC_PROJECT_NAMESPACE {
void registerKeelModules() noexcept;
void registerStudioModules() noexcept;
}

#ifdef _WIN32
int WinMain() {
	auto const argc = __argc;
	auto const argv = const_cast<const char**>(__argv);
#else
int main(int argc, const char **argv) {
#endif
	OX_INIT_DEBUG_LOGGER(loggerConn, OLYMPIC_PROJECT_NAME " " OLYMPIC_APP_NAME)
#if OLYMPIC_LOAD_KEEL_MODULES
	OLYMPIC_PROJECT_NAMESPACE::registerKeelModules();
#endif
#if OLYMPIC_LOAD_STUDIO_MODULES
	OLYMPIC_PROJECT_NAMESPACE::registerStudioModules();
#endif
	auto const err = olympic::run(OLYMPIC_PROJECT_NAME, OLYMPIC_APP_NAME, OLYMPIC_PROJECT_DATADIR, argc, argv);
	oxAssert(err, "Something went wrong...");
	if (err) {
		oxErrf("Failure: {}\n", toStr(err));
	}
	return static_cast<int>(err);
}
