# Nostalgia

## Prerequisites

* Install GCC, Clang, or Visual Studio with C++20 support
* Install [devkitPro](https://devkitpro.org/wiki/Getting_Started) to build for GBA
* Install Python 3
* Install Ninja, Make, and CMake
* Consider also installing ccache for faster subsequent build times

### Debian

For Debian (and probably other Linux distros, but the package names will
probably differ), install the following additional packages:
* pkg-config
* xorg-dev
* libgtk-3-dev

## Build

Build options: release, debug, asan, gba, gba-debug

	make purge configure-{gba,release,debug} install

## Run

### Studio

	make run-studio

### Native Platform

	make run

### GBA

	make gba-run

## Contributing

Please read the [Developer Handbook](developer-handbook.md) for information on
coding standards.
