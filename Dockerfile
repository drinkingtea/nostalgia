FROM fedora:36

RUN dnf update -y

###############################################################################
# Install gosu

RUN dnf install -y curl
RUN curl -o /usr/local/bin/gosu -SL "https://github.com/tianon/gosu/releases/download/1.10/gosu-amd64"
RUN chmod +x /usr/local/bin/gosu

###############################################################################
# Install dev tools

RUN dnf install -y clang \
                   llvm \
                   libasan \
                   mingw64-gcc-c++ \
                   cmake \
                   make \
                   git \
                   vim \
                   sudo \
                   fuse-devel \
                   findutils \
                   ninja-build \
                   libcxx-devel libcxxabi-devel \
                   unzip \
                   ccache \
                   pacman \
                   python3-pip \
                   libglvnd-devel \
                   gtk3-devel
RUN pip install conan

###############################################################################
# Install devkitARM

RUN pacman-key --init
RUN pacman-key --recv BC26F752D25B92CE272E0F44F7FD5492264BB9D0 --keyserver keyserver.ubuntu.com
RUN pacman-key --lsign BC26F752D25B92CE272E0F44F7FD5492264BB9D0
RUN curl -o devkitpro-keyring.pkg.tar.xz https://pkg.devkitpro.org/devkitpro-keyring.pkg.tar.xz
RUN pacman -Syu --noconfirm
RUN pacman -U --noconfirm devkitpro-keyring.pkg.tar.xz
ADD devenv/pacman.conf /etc/pacman.conf
RUN pacman -Syu --noconfirm gba-dev
ENV DEVKITPRO /opt/devkitpro
ENV DEVKITARM /opt/devkitpro/devkitARM

###############################################################################
# Setup sudoers

ADD devenv/sudoers /etc/sudoers

###############################################################################
# Setup working directory

RUN mkdir /usr/src/project
WORKDIR /usr/src/project

###############################################################################
# Setup entrypoint

ADD devenv/entrypoint.sh /
ENTRYPOINT ["/entrypoint.sh"]

ENV CC clang
ENV CXX clang++
ENV VCPKG_DIR_BASE /var/vcpkg/
