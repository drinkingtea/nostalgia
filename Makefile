BC_VAR_PROJECT_NAME=nostalgia
BUILDCORE_PATH=deps/buildcore
include ${BUILDCORE_PATH}/base.mk

ifeq ($(BC_VAR_OS),darwin)
	NOSTALGIA_STUDIO=./build/${BC_VAR_CURRENT_BUILD}/bin/${BC_VAR_PROJECT_NAME}-studio.app/Contents/MacOS/${BC_VAR_PROJECT_NAME}-studio
	MGBA=/Applications/mGBA.app/Contents/MacOS/mGBA
else
	NOSTALGIA_STUDIO=./build/${BC_VAR_CURRENT_BUILD}/bin/${BC_VAR_PROJECT_NAME}-studio
	MGBA=mgba-qt
endif

.PHONY: pkg-gba
pkg-gba: build
	${BC_CMD_ENVRUN} ${BC_PY3} ./scripts/pkg-gba.py sample_project ${BC_VAR_PROJECT_NAME}

.PHONY: run
run: build
	./build/${BC_VAR_CURRENT_BUILD}/bin/${BC_VAR_PROJECT_NAME} sample_project
.PHONY: run-studio
run-studio: build
	${NOSTALGIA_STUDIO}
.PHONY: gba-run
gba-run: pkg-gba
	${MGBA} ${BC_VAR_PROJECT_NAME}.gba
.PHONY: debug
debug: build
	${BC_CMD_HOST_DEBUGGER} ./build/${BC_VAR_CURRENT_BUILD}/bin/${BC_VAR_PROJECT_NAME} sample_project
.PHONY: debug-studio
debug-studio: build
	${BC_CMD_HOST_DEBUGGER} ${NOSTALGIA_STUDIO}

.PHONY: configure-gba
configure-gba:
	${BC_CMD_SETUP_BUILD} --toolchain=deps/gbabuildcore/cmake/modules/GBA.cmake --target=gba --current_build=0 --build_type=release --build_root=${BC_VAR_BUILD_PATH}

.PHONY: configure-gba-debug
configure-gba-debug:
	${BC_CMD_SETUP_BUILD} --toolchain=deps/gbabuildcore/cmake/modules/GBA.cmake --target=gba --current_build=0 --build_type=debug --build_root=${BC_VAR_BUILD_PATH}
