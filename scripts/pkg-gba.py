#! /usr/bin/env python3

#
#  Copyright 2016 - 2023 gary@drinkingtea.net
#
#  This Source Code Form is subject to the terms of the Mozilla Public
#  License, v. 2.0. If a copy of the MPL was not distributed with this
#  file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

import platform
import shutil
import subprocess
import sys

os = platform.system().lower()
arch = platform.machine()
host_env = f'{os}-{arch}'

# get current build type
with open(".current_build","r") as f:
    current_build = f.readlines()[0]

project_dir = sys.argv[1]
project_name = sys.argv[2]
bin = f'./build/{host_env}-{current_build}/bin/'
project_bin = f'build/gba-release/bin/{project_name}.bin'
project_gba = f'{project_name}.gba'

shutil.copyfile(project_bin, project_gba)
subprocess.run([
    f'{bin}/{project_name}-pack', '-src', project_dir, '-rom-bin', project_gba])
subprocess.run(['gbafix', project_gba])
