/*
 * Copyright 2015 - 2022 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "typestore.hpp"
#include "desctypes.hpp"

namespace ox {

template<typename ReaderBase>
class TypeDescReader: public ReaderBase {
	private:
		TypeStore m_typeStore;

	public:
		constexpr TypeDescReader(const uint8_t *buff, std::size_t buffLen) noexcept;

		[[nodiscard]]
		constexpr const auto &typeStore() const noexcept;

};

template<typename ReaderBase>
constexpr TypeDescReader<ReaderBase>::TypeDescReader(const uint8_t *buff, std::size_t buffLen) noexcept:
	ReaderBase(buff, buffLen) {
}

template<typename ReaderBase>
constexpr const auto &TypeDescReader<ReaderBase>::typeStore() const noexcept {
	return m_typeStore;
}

template<typename ReaderBase, typename T>
constexpr int readMCDef(const uint8_t *buff, std::size_t buffLen, T *val) noexcept {
	TypeDescReader<ReaderBase> reader(buff, buffLen);
	return model(&reader, val);
}

}
