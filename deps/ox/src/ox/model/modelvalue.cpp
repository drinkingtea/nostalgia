/*
 * Copyright 2015 - 2022 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "modelvalue.hpp"

namespace ox {

static_assert([]() -> ox::Error {
	ox::ModelValue v;
	oxReturnError(v.setType<int32_t>());
	if (v.type() != ModelValue::Type::SignedInteger32) {
		return OxError(1, "type is wrong");
	}
	//oxReturnError(v.set<int32_t>(5));
	return {};
}() == OxError(0));

}
