/*
 * Copyright 2015 - 2022 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include <ox/std/byteswap.hpp>
#include <ox/std/bstring.hpp>
#include <ox/std/memory.hpp>
#include <ox/std/string.hpp>
#include <ox/std/trace.hpp>
#include <ox/std/types.hpp>
#include <ox/std/vector.hpp>

#include "desctypes.hpp"
#include "fieldcounter.hpp"
#include "optype.hpp"
#include "types.hpp"

namespace ox {

namespace detail {

template<bool>
struct BoolWrapper {
};

template<int>
struct IntWrapper {
};

}

template<typename T, typename = detail::BoolWrapper<true>>
struct preloadable: false_type {
};

template<typename T>
struct preloadable<T, detail::BoolWrapper<T::Preloadable>> {
	static constexpr bool value = T::Preloadable;
};

// cannot be done until C++20
//struct PseudoString {
//	constexpr PseudoString(const char* = "") noexcept {}
//};
//
//template<PseudoString>
//struct StringWrapper {
//};
//
//template<typename T, typename = StringWrapper<"">>
//struct ModelTypeName {
//	static constexpr const char *value = "";
//};
//
//template<typename T>
//struct ModelTypeName<T, detail::StringWrapper<T::TypeName>> {
//	static constexpr const char *value = T::TypeName;
//};

}
