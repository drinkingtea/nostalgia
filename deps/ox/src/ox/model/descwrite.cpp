/*
 * Copyright 2015 - 2022 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "descwrite.hpp"

namespace ox {

namespace detail {

struct preloadable_type {
	static constexpr auto Preloadable = true;
};

struct non_preloadable_type {
	static constexpr auto Preloadable = false;
};

struct non_preloadable_type2 {
};

static_assert(preloadable<preloadable_type>::value);
static_assert(!preloadable<non_preloadable_type>::value);
static_assert(!preloadable<non_preloadable_type2>::value);

}


static_assert([] {
	return detail::indirectionLevels_v<int> == 0;
}(), "indirectionLevels broken: indirectionLevels(int)");

static_assert([] {
	return detail::indirectionLevels_v<int*> == 1;
}(), "indirectionLevels broken: indirectionLevels(int*)");

static_assert([] {
	return detail::indirectionLevels_v<int[2]> == 1;
}(), "indirectionLevels broken: indirectionLevels(int[])");

static_assert([] {
	return detail::indirectionLevels_v<int**> == 2;
}(), "indirectionLevels broken: indirectionLevels(int[])");

}
