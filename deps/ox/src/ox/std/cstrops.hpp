/*
 * Copyright 2015 - 2023 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "types.hpp"
#include "typetraits.hpp"

template<typename T1, typename T2>
constexpr T1 ox_strncpy(T1 dest, T2 src, std::size_t maxLen) noexcept {
	using T1Type = typename ox::remove_reference<decltype(dest[0])>::type;
	std::size_t i = 0;
	while (i < maxLen && src[i]) {
		dest[i] = static_cast<T1Type>(src[i]);
		++i;
	}
	// set null terminator
	dest[i] = 0;
	return dest;
}

[[nodiscard]]
constexpr auto ox_strnlen(const char *str1, std::size_t maxLen) noexcept {
	std::size_t len = 0;
	for (; len < maxLen && str1[len]; len++);
	return len;
}

template<typename T>
[[nodiscard]]
constexpr auto ox_strlen(T const&str1) noexcept {
	std::size_t len = 0;
	for (; str1[len]; len++);
	return len;
}

template<typename T1, typename T2>
[[nodiscard]]
constexpr int ox_strcmp(const T1 &str1, const T2 &str2) noexcept {
	auto retval = 0;
	auto i = 0u;
	while (str1[i] || str2[i]) {
		if (str1[i] < str2[i]) {
			retval = -1;
			break;
		} else if (str1[i] > str2[i]) {
			retval = 1;
			break;
		}
		i++;
	}
	return retval;
}

template<typename T1, typename T2>
[[nodiscard]]
constexpr int ox_strncmp(T1 const&str1, T2 const&str2, const std::size_t maxLen) noexcept {
	auto retval = 0;
	std::size_t i = 0;
	while (i < maxLen && (str1[i] || str2[i])) {
		if (str1[i] < str2[i]) {
			retval = -1;
			break;
		} else if (str1[i] > str2[i]) {
			retval = 1;
			break;
		}
		i++;
	}
	return retval;
}

[[nodiscard]]
constexpr const char *ox_strchr(const char *str, int character, std::size_t maxLen = 0xFFFFFFFF) noexcept {
	for (std::size_t i = 0; i <= maxLen; i++) {
		if (str[i] == character) {
			return &str[i];
		} else if (str[i] == 0) {
			return nullptr;
		}
	}
	return nullptr;
}

[[nodiscard]]
constexpr char *ox_strchr(char *str, int character, std::size_t maxLen = 0xFFFFFFFF) noexcept {
	for (std::size_t i = 0; i < maxLen; i++) {
		if (str[i] == character) {
			return &str[i];
		} else if (str[i] == 0) {
			return nullptr;
		}
	}
	return nullptr;
}

[[nodiscard]]
constexpr int ox_lastIndexOf(const auto &str, int character, std::size_t maxLen = 0xFFFFFFFF) noexcept {
	int retval = -1;
	for (std::size_t i = 0; i < maxLen && str[i]; i++) {
		if (str[i] == character) {
			retval = static_cast<int>(i);
		}
	}
	return retval;
}

template<typename Integer, typename T>
constexpr T ox_itoa(Integer v, T str) noexcept {
	if (v) {
		ox::ResizedInt_t<Integer, 64> mod = 1000000000000000000;
		ox::ResizedInt_t<Integer, 64> val = v;
		constexpr auto base = 10;
		auto it = 0;
		if (val < 0) {
			str[static_cast<std::size_t>(it)] = '-';
			it++;
		}
		while (mod) {
			auto digit = val / mod;
			val %= mod;
			mod /= base;
			if (it || digit) {
				ox::ResizedInt_t<Integer, 64> start = '0';
				if (digit >= 10) {
					start = 'a';
					digit -= 10;
				}
				str[static_cast<std::size_t>(it)] = static_cast<typename ox::remove_reference<decltype(str[0])>::type>(start + digit);
				it++;
			}
		}
		str[static_cast<std::size_t>(it)] = 0;
	} else {
		// 0 is a special case
		str[0] = '0';
		str[1] = 0;
	}
	return str;
}
