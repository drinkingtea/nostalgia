/*
 * Copyright 2015 - 2022 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "trace.hpp"

namespace ox::trace {

void init() {
	oxTraceInitHook();
}

void init(Logger *logger) {
	oxTraceInitHook();
	setLogger(logger);
}

class NullLogger: public Logger {
	public:
		ox::Error send(const TraceMsg&) noexcept final {
			return {};
		}
		ox::Error sendInit(const InitTraceMsg&) noexcept final {
			return {};
		}
};

static NullLogger defaultLogger;
static Logger *logger = &defaultLogger;

void setLogger(Logger *logger) noexcept {
	trace::logger = logger;
}

void send(const TraceMsg &msg) noexcept {
	oxIgnoreError(logger->send(msg));
}

}
