/*
 * Copyright 2015 - 2022 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "math.hpp"

namespace ox {

static_assert(min(2, 1) == 1);
static_assert(min(1, 2) == 1);

static_assert(max(2, 1) == 2);
static_assert(max(1, 2) == 2);

static_assert(clamp(2, 1, 3) == 2);
static_assert(clamp(1, 2, 3) == 2);
static_assert(clamp(3, 1, 2) == 2);

}
