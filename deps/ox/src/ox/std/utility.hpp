/*
 * Copyright 2015 - 2022 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "typetraits.hpp"

namespace ox {

template<class T>
constexpr T &&forward(remove_reference_t<T> &t) noexcept {
	return static_cast<T&&>(t);
}

template<class T>
constexpr T &&forward(remove_reference_t<T> &&t) noexcept {
	return static_cast<T&&>(t);
}

}


#if __has_include(<utility>)
#include <utility>
#else
namespace std {
template<typename T>
constexpr typename ox::remove_reference<T>::type &&move(T &&t) noexcept {
	return static_cast<typename ox::remove_reference<T>::type&&>(t);
}
}
#endif
