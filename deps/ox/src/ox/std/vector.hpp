/*
 * Copyright 2015 - 2023 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "array.hpp"
#include "concepts.hpp"
#include "bit.hpp"
#include "error.hpp"
#include "initializerlist.hpp"
#include "iterator.hpp"
#include "math.hpp"
#include "memory.hpp"
#include "stringview.hpp"
#include "types.hpp"
#include "utility.hpp"

namespace ox {

namespace detail {

template<typename T, typename Allocator, std::size_t Size = 1>
struct VectorAllocator {
	static_assert(sizeof(AllocAlias<T>) == sizeof(T));
	static_assert(alignof(AllocAlias<T>) == alignof(T));
	private:
		ox::Array<AllocAlias<T>, Size> m_data = {};
		Allocator m_allocator;
	protected:
		constexpr VectorAllocator() noexcept = default;
		constexpr VectorAllocator(const VectorAllocator&) noexcept = default;
		constexpr VectorAllocator(VectorAllocator&&) noexcept = default;

		constexpr void allocate(T **items, std::size_t cap) noexcept {
			// small vector optimization cannot be done it constexpr, but it doesn't really matter in constexpr
			if (std::is_constant_evaluated() || cap > Size) {
				*items = m_allocator.allocate(cap);
			} else {
				*items = reinterpret_cast<T*>(m_data.data());
			}
		}

		constexpr void moveConstructItemsFrom(
				T **items,
				VectorAllocator *src,
				const std::size_t count,
				const std::size_t cap) noexcept {
			// this totally idiotic redundant check (&& count <= Size) is required to address a bug in devkitARM,
			// try removing it later
			if (cap <= m_data.size() && count <= m_data.size()) {
				for (auto i = 0u; i < count; ++i) {
					const auto dstItem = reinterpret_cast<T*>(&m_data[i]);
					const auto srcItem = reinterpret_cast<T*>(&src->m_data[i]);
					std::construct_at<T>(dstItem, std::move(*srcItem));
				}
				*items = reinterpret_cast<T*>(m_data.data());
			}
		}

		constexpr void moveItemsFrom(
				T **items,
				VectorAllocator *src,
				const std::size_t count,
				const std::size_t cap) noexcept {
			// this totally idiotic redundant check (&& count <= Size) is required to address a bug in devkitARM,
			// try removing it later
			if (cap <= m_data.size() && count <= m_data.size()) {
				for (std::size_t i = 0; i < count; ++i) {
					const auto dstItem = reinterpret_cast<T*>(&m_data[i]);
					const auto srcItem = reinterpret_cast<T*>(&src->m_data[i]);
					*dstItem = std::move(*srcItem);
				}
				*items = reinterpret_cast<T*>(m_data.data());
			}
		}

		constexpr void deallocate(T *items, std::size_t cap) noexcept {
			// small vector optimization cannot be done it constexpr, but it doesn't really matter in constexpr
			if (std::is_constant_evaluated() || (items && static_cast<void*>(items) != static_cast<void*>(m_data.data()))) {
				m_allocator.deallocate(items, cap);
			}
		}

};

template<typename T, typename Allocator>
struct VectorAllocator<T, Allocator, 0> {
	private:
		Allocator m_allocator;
	protected:
		constexpr VectorAllocator() noexcept = default;
		constexpr VectorAllocator(const VectorAllocator&) noexcept = default;
		constexpr VectorAllocator(VectorAllocator&&) noexcept = default;

		constexpr void allocate(T **items, std::size_t cap) noexcept {
			*items = m_allocator.allocate(cap);
		}

		[[maybe_unused]]
		constexpr void moveConstructItemsFrom(
				T**,
				VectorAllocator*,
				const std::size_t,
				const std::size_t) noexcept {
		}

		[[maybe_unused]]
		constexpr void moveItemsFrom(T**, VectorAllocator*, const std::size_t, const std::size_t) noexcept {
		}

		constexpr void deallocate(T *items, std::size_t cap) noexcept {
			if (items) {
				m_allocator.deallocate(items, cap);
			}
		}

};

}

template<typename T, std::size_t SmallVectorSize = 0, typename Allocator = std::allocator<T>>
class Vector: detail::VectorAllocator<T, Allocator, SmallVectorSize> {

	public:
		using value_type = T;
		using size_type = std::size_t;

		template<typename RefType = T&, typename PtrType = T*, bool reverse = false>
		using iterator = SpanIterator<T, RefType, PtrType, reverse>;

	private:
		static constexpr auto initialCap = SmallVectorSize > 0 ? SmallVectorSize : 50;
		static constexpr auto useNoexcept = ox::is_integral_v<T> || ox::is_pointer_v<T>;
		std::size_t m_size = 0;
		std::size_t m_cap = 0;
		T *m_items = nullptr;

	public:
		constexpr Vector() noexcept = default;

		explicit constexpr Vector(std::size_t size) noexcept;

		constexpr Vector(std::initializer_list<T> list) noexcept;

		constexpr Vector(const Vector &other) noexcept(useNoexcept);

		constexpr Vector(Vector &&other) noexcept;

		constexpr ~Vector();

		constexpr iterator<> begin() noexcept {
			return iterator<>(m_items, 0, m_size);
		}

		constexpr iterator<> end() noexcept {
			return iterator<>(m_items, m_size, m_size);
		}

		[[nodiscard]]
		constexpr iterator<const T&, const T*> begin() const noexcept {
			return iterator<const T&, const T*>(m_items, 0, m_size);
		}

		[[nodiscard]]
		constexpr iterator<const T&, const T*> end() const noexcept {
			return iterator<const T&, const T*>(m_items, m_size, m_size);
		}

		[[nodiscard]]
		constexpr iterator<const T&, const T*> cbegin() const noexcept {
			return iterator<const T&, const T*>(m_items, 0, m_size);
		}

		[[nodiscard]]
		constexpr iterator<const T&, const T*> cend() const noexcept {
			return iterator<const T&, const T*>(m_items, m_size, m_size);
		}

		[[nodiscard]]
		constexpr iterator<T&, T*, true> rbegin() noexcept {
			return iterator<T&, T*, true>(m_items, m_size - 1, m_size);
		}

		[[nodiscard]]
		constexpr iterator<T&, T*, true> rend() noexcept {
			return iterator<T&, T*, true>(m_items, MaxValue<size_type>, m_size);
		}

		[[nodiscard]]
		constexpr iterator<const T&, const T*, true> crbegin() const noexcept {
			return iterator<const T&, const T*, true>(m_items, m_size - 1, m_size);
		}

		[[nodiscard]]
		constexpr iterator<const T&, const T*, true> crend() const noexcept {
			return iterator<const T&, const T*, true>(m_items, MaxValue<size_type>, m_size);
		}

		[[nodiscard]]
		constexpr iterator<const T&, const T*, true> rbegin() const noexcept {
			return iterator<const T&, const T*, true>(m_items, m_size - 1, m_size);
		}

		[[nodiscard]]
		constexpr iterator<const T&, const T*, true> rend() const noexcept {
			return iterator<const T&, const T*, true>(m_items, MaxValue<size_type>, m_size);
		}

		constexpr bool operator==(const Vector &other) const noexcept(useNoexcept);

		constexpr Vector &operator=(const Vector &other) noexcept(useNoexcept);

		constexpr Vector &operator=(Vector &&other) noexcept;

		constexpr T &operator[](std::size_t i) noexcept;

		constexpr const T &operator[](std::size_t i) const noexcept;

		[[nodiscard]]
		constexpr Result<T*> front() noexcept;

		[[nodiscard]]
		constexpr Result<const T*> front() const noexcept;

		[[nodiscard]]
		constexpr Result<T*> back() noexcept;

		[[nodiscard]]
		constexpr Result<const T*> back() const noexcept;

		[[nodiscard]]
		constexpr std::size_t size() const noexcept;

		[[nodiscard]]
		constexpr bool empty() const noexcept;

		constexpr void clear() noexcept(useNoexcept);

		constexpr void resize(std::size_t size) noexcept(useNoexcept);

		[[nodiscard]]
		constexpr T *data() noexcept {
			return m_items;
		}

		[[nodiscard]]
		constexpr const T *data() const noexcept {
			return m_items;
		}

		[[nodiscard]]
		constexpr bool contains(MaybeSV_t<T> const&) const noexcept(useNoexcept);

		constexpr iterator<T&, T*, false> insert(
				std::size_t pos, std::size_t cnt, MaybeSV_t<T> const&val) noexcept(useNoexcept);

		constexpr iterator<T&, T*, false> insert(std::size_t pos, MaybeSV_t<T> const&val) noexcept(useNoexcept);

		template<typename... Args>
		constexpr iterator<T&, T*, false> emplace(std::size_t pos, Args&&... args) noexcept(useNoexcept);

		template<typename... Args>
		constexpr T &emplace_back(Args&&... args) noexcept(useNoexcept);

		constexpr void push_back(T &&item) noexcept(useNoexcept);

		constexpr void push_back(MaybeSV_t<T> const&item) noexcept(useNoexcept);

		constexpr void pop_back() noexcept(useNoexcept);

		/**
		 * Removes an item from the Vector.
		 * @param pos iterator at the point to remove
		 * @return Error if index is out of bounds
		 */
		constexpr Result<iterator<T&, T*, false>> erase(const iterator<> &pos) noexcept(useNoexcept);

		/**
		 * Removes an item from the Vector.
		 * @param pos position of item to remove
		 * @return Error if index is out of bounds
		 */
		constexpr Result<iterator<T&, T*, false>> erase(std::size_t pos) noexcept(useNoexcept);

		/**
		 * Moves the last item in the Vector to position pos and decrements the
		 * size by 1.
		 * @param pos position of item to remove
		 * @return Error if index is out of bounds
		 */
		constexpr Error unordered_erase(std::size_t pos) noexcept(useNoexcept);

		constexpr void reserve(std::size_t cap) noexcept(useNoexcept);

	private:
		constexpr void reserveInsert(
				std::size_t cap, std::size_t pos, std::size_t offset = 1) noexcept(useNoexcept);

};

template<typename T, std::size_t SmallVectorSize, typename Allocator, typename RefType, bool reverse>
using VectorIt = typename Vector<T, SmallVectorSize, Allocator>::template iterator<RefType, reverse>;

template<typename T, std::size_t SmallVectorSize, typename Allocator, typename RefType, bool reverse>
constexpr VectorIt<T, SmallVectorSize, Allocator, RefType, reverse> operator+(
		std::size_t n,
		const VectorIt<T, SmallVectorSize, Allocator, RefType, reverse> &a) {
	return a + n;
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
constexpr Vector<T, SmallVectorSize, Allocator>::Vector(std::size_t size) noexcept {
	m_size = size;
	m_cap = m_size;
	this->allocate(&m_items, m_cap);
	for (std::size_t i = 0; i < size; ++i) {
		std::construct_at(&m_items[i]);
	}
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
constexpr Vector<T, SmallVectorSize, Allocator>::Vector(std::initializer_list<T> list) noexcept {
	for (auto &item : list) {
		emplace_back(std::move(item));
	}
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
constexpr Vector<T, SmallVectorSize, Allocator>::Vector(const Vector &other) noexcept(useNoexcept) {
	m_size = other.m_size;
	m_cap = other.m_cap;
	this->allocate(&m_items, other.m_cap);
	for (std::size_t i = 0; i < m_size; ++i) {
		std::construct_at(&m_items[i], other.m_items[i]);
	}
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
constexpr Vector<T, SmallVectorSize, Allocator>::Vector(Vector &&other) noexcept {
	m_size = other.m_size;
	m_cap = other.m_cap;
	m_items = other.m_items;
	this->moveConstructItemsFrom(&m_items, &other, m_size, m_cap);
	other.m_size = 0;
	other.m_cap = 0;
	other.m_items = nullptr;
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
constexpr Vector<T, SmallVectorSize, Allocator>::~Vector() {
	clear();
	this->deallocate(m_items, m_cap);
	m_items = nullptr;
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
constexpr bool Vector<T, SmallVectorSize, Allocator>::operator==(
		const Vector &other) const noexcept(useNoexcept) {
	if (m_size != other.m_size) {
		return false;
	}
	for (std::size_t i = 0; i < m_size; i++) {
		if (!(m_items[i] == other.m_items[i])) {
			return false;
		}
	}
	return true;
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
constexpr Vector<T, SmallVectorSize, Allocator> &Vector<T, SmallVectorSize, Allocator>::operator=(
		const Vector &other) noexcept(useNoexcept) {
	if (this != &other) {
		clear();
		this->deallocate(m_items, m_cap);
		m_items = nullptr;
		m_size = other.m_size;
		m_cap = other.m_cap;
		this->allocate(&m_items, other.m_cap);
		for (std::size_t i = 0; i < m_size; i++) {
			std::construct_at(&m_items[i], other.m_items[i]);
		}
	}
	return *this;
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
constexpr Vector<T, SmallVectorSize, Allocator> &Vector<T, SmallVectorSize, Allocator>::operator=(
		Vector &&other) noexcept {
	if (this != &other) {
		clear();
		this->deallocate(m_items, m_cap);
		m_size = other.m_size;
		m_cap = other.m_cap;
		m_items = other.m_items;
		this->moveItemsFrom(&m_items, &other, m_size, m_cap);
		other.m_size = 0;
		other.m_cap = 0;
		other.m_items = nullptr;
	}
	return *this;
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
constexpr T &Vector<T, SmallVectorSize, Allocator>::operator[](std::size_t i) noexcept {
	return m_items[i];
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
constexpr const T &Vector<T, SmallVectorSize, Allocator>::operator[](std::size_t i) const noexcept {
	return m_items[i];
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
constexpr Result<T*> Vector<T, SmallVectorSize, Allocator>::front() noexcept {
	if (!m_size) {
		return {nullptr, OxError(1)};
	}
	return &m_items[0];
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
constexpr Result<const T*> Vector<T, SmallVectorSize, Allocator>::front() const noexcept {
	if (!m_size) {
		return {nullptr, OxError(1)};
	}
	return &m_items[0];
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
constexpr Result<T*> Vector<T, SmallVectorSize, Allocator>::back() noexcept {
	if (!m_size) {
		return {nullptr, OxError(1)};
	}
	return &m_items[m_size - 1];
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
constexpr Result<const T*> Vector<T, SmallVectorSize, Allocator>::back() const noexcept {
	if (!m_size) {
		return {nullptr, OxError(1)};
	}
	return &m_items[m_size - 1];
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
constexpr std::size_t Vector<T, SmallVectorSize, Allocator>::size() const noexcept {
	return m_size;
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
constexpr bool Vector<T, SmallVectorSize, Allocator>::empty() const noexcept {
	return !m_size;
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
constexpr void Vector<T, SmallVectorSize, Allocator>::clear() noexcept(useNoexcept) {
	if constexpr(is_class<T>()) {
		for (std::size_t i = 0; i < m_size; ++i) {
			m_items[i].~T();
		}
	}
	m_size = 0;
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
constexpr void Vector<T, SmallVectorSize, Allocator>::resize(std::size_t size) noexcept(useNoexcept) {
	if (m_cap < size) {
		reserve(size * 2);
	}
	if (m_size < size) {
		for (std::size_t i = m_size; i < size; i++) {
			std::construct_at(&m_items[i]);
		}
	} else {
		for (std::size_t i = size; i < m_size; i++) {
			m_items[i].~T();
		}
	}
	m_size = size;
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
constexpr bool Vector<T, SmallVectorSize, Allocator>::contains(MaybeSV_t<T> const&v) const noexcept(useNoexcept) {
	for (std::size_t i = 0; i < m_size; i++) {
		if (m_items[i] == v) {
			return true;
		}
	}
	return false;
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
constexpr typename Vector<T, SmallVectorSize, Allocator>::template iterator<T&, T*, false>
Vector<T, SmallVectorSize, Allocator>::insert(
		std::size_t pos, std::size_t cnt, MaybeSV_t<T> const&val) noexcept(useNoexcept) {
	if (m_size + cnt > m_cap) {
		reserveInsert(m_cap ? m_size + cnt : initialCap, pos, cnt);
		if (pos < m_size) {
			m_items[pos] = val;
		} else {
			for (auto i = 0u; i < cnt; ++i) {
				std::construct_at(&m_items[pos + i], val);
			}
		}
	} else {
		if (pos < m_size) {
			for (auto i = m_size + cnt - 1; i > pos; --i) {
				std::construct_at(&m_items[i], std::move(m_items[i - cnt]));
			}
			m_items[pos] = val;
		} else {
			for (auto i = 0u; i < cnt; ++i) {
				std::construct_at(&m_items[pos + i], val);
			}
		}
	}
	++m_size;
	return begin() + pos;
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
constexpr typename Vector<T, SmallVectorSize, Allocator>::template iterator<T&, T*, false>
Vector<T, SmallVectorSize, Allocator>::insert(std::size_t pos, MaybeSV_t<T> const&val) noexcept(useNoexcept) {
	if (m_size == m_cap) {
		reserveInsert(m_cap ? m_cap * 2 : initialCap, pos);
		if (pos < m_size) {
			m_items[pos] = val;
		} else {
			std::construct_at(&m_items[pos], val);
		}
	} else {
		if (pos < m_size) {
			for (auto i = m_size; i > pos; --i) {
				std::construct_at(&m_items[i], std::move(m_items[i - 1]));
			}
			m_items[pos] = val;
		} else {
			std::construct_at(&m_items[pos], val);
		}
	}
	++m_size;
	return begin() + pos;
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
template<typename... Args>
constexpr typename Vector<T, SmallVectorSize, Allocator>::template iterator<T&, T*, false>
Vector<T, SmallVectorSize, Allocator>::emplace(std::size_t pos, Args&&... args) noexcept(useNoexcept) {
	if (m_size == m_cap) {
		reserveInsert(m_cap ? m_cap * 2 : initialCap, pos);
		if (pos < m_size) {
			m_items[pos].~T();
		}
		std::construct_at(&m_items[pos], ox::forward<Args>(args)...);
	} else {
		if (pos < m_size) {
			for (auto i = m_size; i > pos; --i) {
				std::construct_at(&m_items[i], std::move(m_items[i - 1]));
			}
			m_items[pos].~T();
		}
		std::construct_at(&m_items[pos], ox::forward<Args>(args)...);
	}
	++m_size;
	return begin() + pos;
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
template<typename... Args>
constexpr T &Vector<T, SmallVectorSize, Allocator>::emplace_back(Args&&... args) noexcept(useNoexcept) {
	if (m_size == m_cap) {
		reserve(m_cap ? m_cap * 2 : initialCap);
	}
	auto out = std::construct_at(&m_items[m_size], ox::forward<Args>(args)...);
	++m_size;
	return *out;
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
constexpr void Vector<T, SmallVectorSize, Allocator>::push_back(T &&item) noexcept(useNoexcept) {
	if (m_size == m_cap) {
		reserve(m_cap ? m_cap * 2 : initialCap);
	}
	std::construct_at(&m_items[m_size], std::move(item));
	++m_size;
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
constexpr void Vector<T, SmallVectorSize, Allocator>::push_back(MaybeSV_t<T> const&item) noexcept(useNoexcept) {
	if (m_size == m_cap) {
		reserve(m_cap ? m_cap * 2 : initialCap);
	}
	std::construct_at(&m_items[m_size], item);
	++m_size;
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
constexpr void Vector<T, SmallVectorSize, Allocator>::pop_back() noexcept(useNoexcept) {
	--m_size;
	m_items[m_size].~T();
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
constexpr Result<typename Vector<T, SmallVectorSize, Allocator>::template iterator<T&, T*, false>>
Vector<T, SmallVectorSize, Allocator>::erase(const iterator<> &pos) noexcept(useNoexcept) {
	return erase(pos.offset());
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
constexpr Result<typename Vector<T, SmallVectorSize, Allocator>::template iterator<T&, T*, false>>
Vector<T, SmallVectorSize, Allocator>::erase(std::size_t pos) noexcept(useNoexcept) {
	if (pos >= m_size) {
		return OxError(1, "Vector::erase failed: pos is greater than Vector size");
	}
	--m_size;
	for (auto i = pos; i < m_size; ++i) {
		m_items[i] = std::move(m_items[i + 1]);
	}
	m_items[m_size].~T();
	return begin() + pos;
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
constexpr Error Vector<T, SmallVectorSize, Allocator>::unordered_erase(std::size_t pos)
	noexcept(useNoexcept) {
	if (pos >= m_size) {
		return OxError(1);
	}
	--m_size;
	m_items[pos] = std::move(m_items[m_size]);
	m_items[m_size].~T();
	return OxError(0);
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
constexpr void Vector<T, SmallVectorSize, Allocator>::reserve(std::size_t cap) noexcept(useNoexcept) {
	if (cap <= m_cap) {
		return;
	}
	const auto oldItems = m_items;
	const auto oldCap = m_cap;
	m_cap = cap;
	this->allocate(&m_items, cap);
	if (oldItems) { // move over old items
		const auto itRange = ox::min(cap, m_size);
		for (std::size_t i = 0; i < itRange; ++i) {
			std::construct_at(&m_items[i], std::move(oldItems[i]));
			oldItems[i].~T();
		}
		this->deallocate(oldItems, oldCap);
	}
}

template<typename T, std::size_t SmallVectorSize, typename Allocator>
constexpr void Vector<T, SmallVectorSize, Allocator>::reserveInsert(
		std::size_t cap,
		std::size_t pos,
		std::size_t offset) noexcept(useNoexcept) {
	if (cap <= m_cap) {
		return;
	}
	const auto oldItems = m_items;
	const auto oldCap = m_cap;
	m_cap = cap;
	this->allocate(&m_items, cap);
	if (oldItems) { // move over old items
		auto itRange = ox::min(m_size, pos);
		for (std::size_t i = 0; i < itRange; ++i) {
			std::construct_at(&m_items[i], std::move(oldItems[i]));
			oldItems[i].~T();
		}
		itRange = m_size;
		for (std::size_t i = pos; i < itRange; ++i) {
			std::construct_at(&m_items[i + offset], std::move(oldItems[i]));
			oldItems[i].~T();
		}
		this->deallocate(oldItems, oldCap);
	}
}


template<typename PlatSpec, typename T>
[[nodiscard]]
constexpr auto alignOf(const Vector<T>&) noexcept {
	const typename PlatSpec::size_t i = 0;
	return PlatSpec::alignOf(i);
}

}
