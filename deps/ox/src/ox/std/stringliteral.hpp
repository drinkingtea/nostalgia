/*
 * Copyright 2015 - 2022 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "basestringview.hpp"

namespace ox {

/**
 * StringLiteral is used for functions that want to ensure that they are taking
 * string literals, and not strings outside of the data section of the program
 * that might get deleted.
 * This type cannot force you to use it correctly, so don't give it something
 * that is not a literal.
 * If you do this:
 *		StringLiteral(str.c_str())
 *	the resulting segfault is on you.
 */
class StringLiteral: public detail::BaseStringView {
	public:
		constexpr StringLiteral() noexcept = default;

		constexpr StringLiteral(StringLiteral const&sv) noexcept = default;

		constexpr explicit StringLiteral(std::nullptr_t) noexcept {}

		constexpr explicit StringLiteral(const char *str) noexcept: BaseStringView(str) {}

		constexpr explicit StringLiteral(const char *str, std::size_t len) noexcept: BaseStringView(str, len) {}

		constexpr auto &operator=(StringLiteral const&other) noexcept {
			if (&other != this) {
				set(other.data(), other.len());
			}
			return *this;
		}

		[[nodiscard]]
		constexpr const char *c_str() const noexcept {
			return data();
		}

};

}
