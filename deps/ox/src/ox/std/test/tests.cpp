/*
 * Copyright 2015 - 2023 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#undef NDEBUG

#include <map>
#include <ox/std/uuid.hpp>
#include <ox/std/std.hpp>

static std::map<ox::StringView, ox::Error(*)()> tests = {
	{
		"malloc",
		[] {
			ox::Buffer buff(ox::units::MB);
			ox::heapmgr::initHeap(&buff[0], &buff[buff.size()-1]);
			auto a1 = static_cast<char*>(ox::heapmgr::malloc(5));
			auto a2 = static_cast<char*>(ox::heapmgr::malloc(5));
			oxAssert(a1 >= buff.front().unwrap() && a1 < buff.back().unwrap(), "malloc is broken");
			oxAssert(a2 >= buff.front().unwrap() && a2 < buff.back().unwrap() && a2 > a1 + 5, "malloc is broken");
			return OxError(0);
		}
	},
	{
		"itoa",
		[]() {
			ox::Array<char, 10> buff;
			ox::CharBuffWriter bw(buff);
			oxAssert(ox::itoa(5, bw), "ox::itoa returned Error");
			oxExpect(ox::StringView(buff.data()), ox::StringView("5"));
			oxReturnError(bw.seekp(0));
			oxAssert(ox::itoa(50, bw), "ox::itoa returned Error");
			oxExpect(ox::StringView(buff.data()), ox::StringView("50"));
			oxReturnError(bw.seekp(0));
			oxAssert(ox::itoa(500, bw), "ox::itoa returned Error");
			oxExpect(ox::StringView(buff.data()), ox::StringView("500"));
			return ox::Error{};
		}
	},
	{
		"ABCDEFG != HIJKLMN",
		[]() {
			return OxError(ox_memcmp("ABCDEFG", "HIJKLMN", 7) >= 0);
		}
	},
	{
		"HIJKLMN != ABCDEFG",
		[]() {
			return OxError(ox_memcmp("HIJKLMN", "ABCDEFG", 7) <= 0);
		}
	},
	{
		"ABCDEFG == ABCDEFG",
		[]() {
			return OxError(ox_memcmp("ABCDEFG", "ABCDEFG", 7) != 0);
		}
	},
	{
		"ABCDEFGHI == ABCDEFG",
		[]() {
			return OxError(ox_memcmp("ABCDEFGHI", "ABCDEFG", 7) != 0);
		}
	},
	{
		"BString",
		[]() {
			ox::BString<5> s;
			s += "A";
			s += "B";
			s += 9;
			s += "C";
			oxAssert(s == "AB9C", "BString append broken");
			s = "asdf";
			oxAssert(s == "asdf", "String assign broken");
			oxAssert(s != "aoeu", "String assign broken");
			oxAssert(s.len() == 4, "String assign broken");
			return OxError(0);
		}
	},
	{
		"String",
		[]() {
			ox::String s;
			s += "A";
			s += "B";
			s += 9;
			s += "C";
			oxAssert(s == "AB9C", "String append broken");
			s = "asdf";
			oxAssert(s == "asdf", "String assign broken");
			s += "aoeu";
			oxAssert(s == "asdfaoeu", "String append broken");
			const ox::StringView str = "asdf";
			oxAssert(beginsWith(str, "as"), "String beginsWith is broken");
			oxAssert(beginsWith(str, "asd"), "String beginsWith is broken");
			oxAssert(beginsWith(str, "asdf"), "String beginsWith is broken");
			oxAssert(!beginsWith(str, "aa"), "String beginsWith is broken");
			oxAssert(!beginsWith(str, "aaaaaaa"), "String beginsWith is broken");
			oxAssert(!beginsWith(str, "li"), "String beginsWith is broken");
			oxAssert(!beginsWith(str, "afoif"), "String beginsWith is broken");
			oxAssert(endsWith(str, "df"), "String endsWith is broken");
			oxAssert(!endsWith(str, "awefawe"), "String endsWith is broken");
			oxAssert(!endsWith(str, "eu"), "String endsWith is broken");
			oxAssert(ox::StringView("Write") != ox::String(""), "String / StringView comparison broken");
			oxAssert(ox::String("Write") != ox::StringView(""), "String / StringView comparison broken");
			oxAssert(ox::String("Write") == ox::StringView("Write"), "String / StringView comparison broken");
			oxAssert(ox::String(ox::StringView("Write")) == ox::StringView("Write"), "String / StringView comparison broken");
			return OxError(0);
		}
	},
	{
		"Vector",
		[] {
			ox::Vector<int> v;
			oxAssert(v.size() == 0, "Initial Vector size not 0");
			oxAssert(v.empty(), "Vector::empty() is broken");
			auto insertTest = [&v](int val, [[maybe_unused]] std::size_t size) {
				v.push_back(val);
				oxReturnError(OxError(v.size() != size, "Vector size incorrect"));
				oxReturnError(OxError(v[v.size() - 1] != val, "Vector value wrong"));
				return OxError(0);
			};
			oxAssert(insertTest(42, 1), "Vector insertion failed");
			oxAssert(insertTest(100, 2), "Vector insertion failed");
			oxAssert(insertTest(102, 3), "Vector insertion failed");
			return OxError(0);
		}
	},
	{
		"HashMap",
		[] {
			ox::HashMap<const char*, int> si;
			si["asdf"] = 42;
			si["aoeu"] = 100;
			oxAssert(si["asdf"] == 42, "asdf != 42");
			oxAssert(si["aoeu"] == 100, "aoeu != 100");
			ox::HashMap<int, int> ii;
			ii[4] = 42;
			ii[5] = 100;
			oxAssert(ii[4] == 42, "4 != 42");
			oxAssert(ii[5] == 100, "5 != 100");
			return OxError(0);
		}
	},
	{
		"Serialize-Int",
		[] {
			using BA = ox::Array<char, 4>;
			const auto actual = ox::serialize<uint32_t>(256).unwrap();
			oxOutf("[{}, {}, {}, {}]", static_cast<int>(actual[0]), static_cast<int>(actual[1]), static_cast<int>(actual[2]), static_cast<int>(actual[3]));
			oxExpect(ox::serialize<int32_t>(4).unwrap(), BA({4, 0, 0, 0}));
			oxExpect(ox::serialize<int32_t>(256).unwrap(), BA({0, 1, 0, 0}));
			oxExpect(ox::serialize<int32_t>(257).unwrap(), BA({1, 1, 0, 0}));
			oxExpect(ox::serialize<uint32_t>(4).unwrap(), BA({4, 0, 0, 0}));
			oxExpect(ox::serialize<uint32_t>(256).unwrap(), BA({0, 1, 0, 0}));
			oxExpect(ox::serialize<uint32_t>(257).unwrap(), BA({1, 1, 0, 0}));
			constexpr auto neg1 = static_cast<char>(-1); // ARM64 Linux assumes -1 literals are ints...
			oxExpect(ox::serialize<uint32_t>(0xffff'ffff).unwrap(), BA({neg1, neg1, neg1, neg1}));
			return OxError(0);
		}
	},
	{
		"BufferWriter",
		[] {
			ox::Buffer b;
			ox::BufferWriter w(&b);
			oxAssert(w.write("asdf", 4), "write failed");
			oxExpect(b.size(), 4u);
			oxAssert(w.write("aoeu", 4), "write failed");
			oxExpect(b.size(), 8u);
			oxExpect(ox::StringView(b.data(), b.size()), "asdfaoeu");
			ox::StringView constexpr qwerty = "qwerty";
			oxAssert(w.write(qwerty.data(), qwerty.bytes()), "write failed");
			oxExpect(b.size(), 14u);
			oxExpect(ox::StringView(b.data(), b.size()), "asdfaoeuqwerty");
			return OxError(0);
		}
	},
	{
		"FromHex",
		[] {
			oxExpect(ox::detail::fromHex("01").unwrap(), 0x01);
			oxExpect(ox::detail::fromHex("02").unwrap(), 0x02);
			oxExpect(ox::detail::fromHex("03").unwrap(), 0x03);
			oxExpect(ox::detail::fromHex("04").unwrap(), 0x04);
			oxExpect(ox::detail::fromHex("05").unwrap(), 0x05);
			oxExpect(ox::detail::fromHex("06").unwrap(), 0x06);
			oxExpect(ox::detail::fromHex("07").unwrap(), 0x07);
			oxExpect(ox::detail::fromHex("08").unwrap(), 0x08);
			oxExpect(ox::detail::fromHex("0d").unwrap(), 0x0d);
			oxExpect(ox::detail::fromHex("0e").unwrap(), 0x0e);
			oxExpect(ox::detail::fromHex("0f").unwrap(), 0x0f);
			oxExpect(ox::detail::fromHex("0F").unwrap(), 0x0f);
			oxExpect(ox::detail::fromHex("fF").unwrap(), 0xff);
			oxExpect(ox::detail::fromHex("ff").unwrap(), 0xff);
			oxExpect(ox::detail::fromHex("a0").unwrap(), 0xa0);
			oxExpect(ox::detail::fromHex("93").unwrap(), 0x93);
			oxExpect(ox::detail::fromHex("40").unwrap(), 0x40);
			return OxError(0);
		}
	},
	{
		"ToHex",
		[] {
			oxExpect(ox::detail::toHex(0x01), "01");
			oxExpect(ox::detail::toHex(0x02), "02");
			oxExpect(ox::detail::toHex(0x03), "03");
			oxExpect(ox::detail::toHex(0x04), "04");
			oxExpect(ox::detail::toHex(0x05), "05");
			oxExpect(ox::detail::toHex(0x06), "06");
			oxExpect(ox::detail::toHex(0x07), "07");
			oxExpect(ox::detail::toHex(0x08), "08");
			oxExpect(ox::detail::toHex(0x0d), "0d");
			oxExpect(ox::detail::toHex(0x0e), "0e");
			oxExpect(ox::detail::toHex(0x0f), "0f");
			oxExpect(ox::detail::toHex(0x93), "93");
			oxExpect(ox::detail::toHex(0x40), "40");
			oxExpect(ox::detail::toHex(0xf0), "f0");
			return OxError(0);
		}
	},
	{
		"UUID",
		[] {
			constexpr ox::StringView uuidStr = "8d814442-f46e-4cc3-8edc-ca3c01cc86db";
			oxRequire(uuid, ox::UUID::fromString(uuidStr));
			oxExpect(uuid.toString(), uuidStr);
			oxExpect(ox::UUID{}.isNull(), true);
			oxExpect(ox::UUID::fromString(uuidStr).value.isNull(), false);
			return OxError(0);
		}
	},
	{
		"UUID::generate",
		[] {
			ox::UUID::seedGenerator({1234, 4321});
			oxExpect(ox::UUID::generate().unwrap().toString(), "5c3f4b5e-ccbf-4727-7f03-3053dedc8827");
			oxExpect(ox::UUID::generate().unwrap().toString(), "90d0274a-2774-4afa-88e5-0c1d60ba3abf");
			oxExpect(ox::UUID::generate().unwrap().toString(), "7df77910-841c-48ba-ea2e-44521ac47c2e");
			return OxError(0);
		}
	},
	{
		"StringSplit",
		[] {
			ox::StringView sv = "ab.cd";
			auto list = ox::split(sv, ".");
			oxExpect(list.size(), 2u);
			oxExpect(list[0], "ab");
			oxExpect(list[1], "cd");
			sv = "ab.cd.fg";
			list = ox::split(sv, ".");
			oxExpect(list.size(), 3u);
			oxExpect(list[0], "ab");
			oxExpect(list[1], "cd");
			oxExpect(list[2], "fg");
			sv = "ab.cd.";
			list = ox::split(sv, ".");
			oxExpect(list.size(), 2u);
			oxExpect(list[0], "ab");
			oxExpect(list[1], "cd");
			sv = ".ab.cd.";
			list = ox::split(sv, ".");
			oxExpect(list.size(), 2u);
			oxExpect(list[0], "ab");
			oxExpect(list[1], "cd");
			sv = ".";
			list = ox::split(sv, ".");
			oxExpect(list.size(), 0u);
			sv = ".";
			list = ox::split(sv, ".");
			oxExpect(list.size(), 0u);
			sv = "";
			list = ox::split(sv, ".");
			oxExpect(list.size(), 0u);
			// split by single char
			sv = "ab.cd";
			list = ox::split(sv, '.');
			oxExpect(list.size(), 2u);
			oxExpect(list[0], "ab");
			oxExpect(list[1], "cd");
			sv = "ab.cd.fg";
			list = ox::split(sv, '.');
			oxExpect(list.size(), 3u);
			oxExpect(list[0], "ab");
			oxExpect(list[1], "cd");
			oxExpect(list[2], "fg");
			sv = "ab.cd.";
			list = ox::split(sv, '.');
			oxExpect(list.size(), 2u);
			oxExpect(list[0], "ab");
			oxExpect(list[1], "cd");
			sv = ".ab.cd.";
			list = ox::split(sv, '.');
			oxExpect(list.size(), 2u);
			oxExpect(list[0], "ab");
			oxExpect(list[1], "cd");
			sv = ".";
			list = ox::split(sv, '.');
			oxExpect(list.size(), 0u);
			sv = ".";
			list = ox::split(sv, '.');
			oxExpect(list.size(), 0u);
			sv = "";
			list = ox::split(sv, '.');
			oxExpect(list.size(), 0u);
			return OxError(0);
		}
	},
};

int main(int argc, const char **args) {
	if (argc < 2) {
		oxError("Must specify test to run");
	}
	auto const testName = args[1];
	auto const func = tests.find(testName);
	if (func != tests.end()) {
		oxAssert(func->second(), "Test returned Error");
		return 0;
	}
	return -1;
}
