/*
 * Copyright 2015 - 2023 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#if defined(OX_USE_STDLIB)
#include <bit>
#endif

#include "defines.hpp"
#include "memops.hpp"
#include "types.hpp"
#include "typetraits.hpp"

#if !defined(OX_USE_STDLIB)
namespace std {

template<typename To, typename From>
[[nodiscard]]
constexpr To bit_cast(const From &src) noexcept requires(sizeof(To) == sizeof(From)) {
	return __builtin_bit_cast(To, src);
}

}
#endif

namespace ox {

template<typename To, typename From>
constexpr typename enable_if<sizeof(To) == sizeof(From), To>::type cbit_cast(From src) noexcept {
	To dst = {};
	ox_memcpy(&dst, &src, sizeof(src));
	return dst;
}

template<typename T>
[[nodiscard]]
constexpr T rotl(T i, int shift) noexcept {
	constexpr auto bits = sizeof(i) * 8;
	return (i << static_cast<T>(shift)) | (i >> (bits - static_cast<T>(shift)));
}

template<typename T, typename B = int>
[[nodiscard]]
constexpr T onMask(B bits = sizeof(T) << 3 /* *8 */) noexcept {
	T out = T(0);
	for (B i = 0; i < bits; i++) {
		out |= static_cast<T>(1) << i;
	}
	return out;
}

template<typename T>
constexpr auto MaxValue = onMask<T>(is_signed_v<T> ? sizeof(T) * 8 - 1 : sizeof(T) * 8);

}
