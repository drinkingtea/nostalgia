/*
 * Copyright 2015 - 2022 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "typetraits.hpp"

namespace ox {

template<typename T, typename U>
concept CommonPtrWith = ox::is_same_v<typename ox::remove_pointer<const T*>::type,
                                      typename ox::remove_pointer<const U*>::type>;

template<typename T, typename U>
concept CommonRefWith = ox::is_same_v<typename ox::remove_reference_t<const T&>,
                                      typename ox::remove_reference_t<const U&>>;

template<typename T, typename U>
concept same_as = ox::is_same_v<T, T>;

template<typename T>
concept OxString_c = isOxString_v<T>;

template<typename T>
concept Integral_c = ox::is_integral_v<T>;

}
