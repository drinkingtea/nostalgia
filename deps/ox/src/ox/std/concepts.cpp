/*
 * Copyright 2015 - 2022 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "concepts.hpp"

namespace ox::detail::test {

class TestType;

[[nodiscard]] constexpr bool ok(auto*) noexcept { return false; }
[[nodiscard]] constexpr bool ok(CommonPtrWith<TestType> auto*) noexcept { return true; }

static_assert(ok(static_cast<TestType*>(nullptr)));
static_assert(ok(static_cast<const TestType*>(nullptr)));
static_assert(!ok(static_cast<int*>(nullptr)));
static_assert(!ok(static_cast<const int*>(nullptr)));

}