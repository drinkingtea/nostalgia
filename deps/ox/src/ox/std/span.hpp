/*
 * Copyright 2015 - 2023 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "bit.hpp"
#include "iterator.hpp"
#include "vector.hpp"

namespace ox {

template<typename T>
class SpanView {

	private:
		const T *m_items{};
		const std::size_t m_size{};

	public:
		using value_type = T;
		using size_type = std::size_t;

		template<typename RefType = T const&, typename PtrType = T const*, bool reverse = false>
		using iterator = SpanIterator<T, RefType, PtrType, reverse>;

		constexpr SpanView() noexcept {}

		template<std::size_t sz>
		constexpr SpanView(ox::Array<T, sz> const&a) noexcept:
				m_items(a.data()),
				m_size(a.size()) {
		}

		template<std::size_t sz, typename Allocator>
		constexpr SpanView(ox::Vector<T, sz, Allocator> const&v) noexcept:
				m_items(v.data()),
				m_size(v.size()) {
		}

		template<std::size_t sz>
		constexpr SpanView(const T a[sz]) noexcept:
				m_items(a),
				m_size(sz) {
		}

		constexpr SpanView(const T *a, std::size_t sz) noexcept:
				m_items(a),
				m_size(sz) {
		}

		constexpr iterator<> begin() noexcept {
			return iterator<>(m_items, 0, m_size);
		}

		constexpr iterator<> end() noexcept {
			return iterator<>(m_items, m_size, m_size);
		}

		[[nodiscard]]
		constexpr iterator<const T&, const T*> begin() const noexcept {
			return iterator<const T&, const T*>(m_items, 0, m_size);
		}

		[[nodiscard]]
		constexpr iterator<const T&, const T*> end() const noexcept {
			return iterator<const T&, const T*>(m_items, m_size, m_size);
		}

		[[nodiscard]]
		constexpr iterator<const T&, const T*> cbegin() const noexcept {
			return iterator<const T&, const T*>(m_items, 0, m_size);
		}

		[[nodiscard]]
		constexpr iterator<const T&, const T*> cend() const noexcept {
			return iterator<const T&, const T*>(m_items, m_size, m_size);
		}

		[[nodiscard]]
		constexpr iterator<const T&, const T*, true> crbegin() const noexcept {
			return iterator<const T&, const T*, true>(m_items, m_size - 1, m_size);
		}

		[[nodiscard]]
		constexpr iterator<const T&, const T*, true> crend() const noexcept {
			return iterator<const T&, const T*, true>(m_items, MaxValue<size_type>, m_size);
		}

		[[nodiscard]]
		constexpr iterator<const T&, const T*, true> rbegin() const noexcept {
			return iterator<const T&, const T*, true>(m_items, m_size - 1, m_size);
		}

		[[nodiscard]]
		constexpr iterator<const T&, const T*, true> rend() const noexcept {
			return iterator<const T&, const T*, true>(m_items, MaxValue<size_type>, m_size);
		}

		constexpr const T &operator[](std::size_t i) const noexcept {
			return m_items[i];
		}

		[[nodiscard]]
		constexpr T const*data() const noexcept {
			return m_items;
		}

		[[nodiscard]]
		constexpr std::size_t size() const noexcept {
			return m_size;
		}

		[[nodiscard]]
		constexpr bool empty() const noexcept {
			return m_size == 0;
		}

};

template<typename T>
class Span {

	private:
		T *m_items{};
		const std::size_t m_size{};

	public:
		using value_type = T;
		using size_type = std::size_t;

		template<typename RefType = T&, typename PtrType = T*, bool reverse = false>
		using iterator = SpanIterator<T, RefType, PtrType, reverse>;

		template<std::size_t sz>
		constexpr Span(ox::Array<T, sz> &a) noexcept:
			m_items(a.data()),
			m_size(a.size()) {
		}

		template<std::size_t sz, typename Allocator>
		constexpr Span(ox::Vector<T, sz, Allocator> &v) noexcept:
			m_items(v.data()),
			m_size(v.size()) {
		}

		template<std::size_t sz>
		constexpr Span(T a[sz]) noexcept:
			m_items(a),
			m_size(sz) {
		}

		constexpr Span(T *a, std::size_t sz) noexcept:
			m_items(a),
			m_size(sz) {
		}

		constexpr iterator<> begin() noexcept {
			return iterator<>(m_items, 0, m_size);
		}

		constexpr iterator<> end() noexcept {
			return iterator<>(m_items, m_size, m_size);
		}

		[[nodiscard]]
		constexpr iterator<const T&, const T*> begin() const noexcept {
			return iterator<const T&, const T*>(m_items, 0, m_size);
		}

		[[nodiscard]]
		constexpr iterator<const T&, const T*> end() const noexcept {
			return iterator<const T&, const T*>(m_items, m_size, m_size);
		}

		[[nodiscard]]
		constexpr iterator<const T&, const T*> cbegin() const noexcept {
			return iterator<const T&, const T*>(m_items, 0, m_size);
		}

		[[nodiscard]]
		constexpr iterator<const T&, const T*> cend() const noexcept {
			return iterator<const T&, const T*>(m_items, m_size, m_size);
		}

		[[nodiscard]]
		constexpr iterator<T&, T*, true> rbegin() noexcept {
			return iterator<T&, T*, true>(m_items, m_size - 1, m_size);
		}

		[[nodiscard]]
		constexpr iterator<T&, T*, true> rend() noexcept {
			return iterator<T&, T*, true>(m_items, MaxValue<size_type>, m_size);
		}

		[[nodiscard]]
		constexpr iterator<const T&, const T*, true> crbegin() const noexcept {
			return iterator<const T&, const T*, true>(m_items, m_size - 1, m_size);
		}

		[[nodiscard]]
		constexpr iterator<const T&, const T*, true> crend() const noexcept {
			return iterator<const T&, const T*, true>(m_items, MaxValue<size_type>, m_size);
		}

		[[nodiscard]]
		constexpr iterator<const T&, const T*, true> rbegin() const noexcept {
			return iterator<const T&, const T*, true>(m_items, m_size - 1, m_size);
		}

		[[nodiscard]]
		constexpr iterator<const T&, const T*, true> rend() const noexcept {
			return iterator<const T&, const T*, true>(m_items, MaxValue<size_type>, m_size);
		}

		constexpr const T &operator[](std::size_t i) const noexcept {
			return m_items[i];
		}

		[[nodiscard]]
		constexpr auto data() const noexcept {
			return m_items;
		}

		[[nodiscard]]
		constexpr std::size_t size() const noexcept {
			return m_size;
		}

		[[nodiscard]]
		constexpr bool empty() const noexcept {
			return m_size == 0;
		}

};

}
