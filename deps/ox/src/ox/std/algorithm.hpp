/*
 * Copyright 2015 - 2022 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

namespace ox {

template<typename It, typename T>
constexpr It find(It begin, It end, const T &value) {
	for (; begin != end; ++begin) {
		if (*begin == value) {
			return begin;
		}
	}
	return end;
}

template<typename It>
constexpr It find_if(It begin, It end, auto predicate) {
	for (; begin != end; ++begin) {
		if (predicate(*begin)) {
			return begin;
		}
	}
	return end;
}

template<typename It, typename Size, typename OutIt>
constexpr OutIt copy_n(It in, Size cnt, OutIt out) {
	for (Size i = 0; i < cnt; ++i) {
		*out = *in;
		++out;
		++in;
	}
	return out;
}

}