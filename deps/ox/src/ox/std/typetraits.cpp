/*
 * Copyright 2015 - 2022 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "typetraits.hpp"

namespace ox {

template<bool moveable, bool copyable>
struct ConstructableTest {
	constexpr ConstructableTest(int) noexcept {}
	constexpr ConstructableTest(ConstructableTest&) noexcept requires(copyable) {}
	constexpr ConstructableTest(ConstructableTest&&) noexcept requires(moveable) {}
};

static_assert(!is_move_constructible_v<ConstructableTest<false, false>>);
static_assert(is_move_constructible_v<ConstructableTest<true, false>>);
static_assert(!is_move_constructible_v<ConstructableTest<false, true>>);
static_assert(is_move_constructible_v<ConstructableTest<true, true>>);

}