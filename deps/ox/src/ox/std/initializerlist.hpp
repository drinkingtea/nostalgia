/*
 * Copyright 2015 - 2022 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#if __has_include(<initializer_list>)
#include <initializer_list>
#else

#include "types.hpp"

namespace std {

template<typename T>
class initializer_list {
	private:
		T *m_begin = nullptr;
		size_t m_size = 0;

	public:
		constexpr initializer_list() noexcept = default;

		constexpr size_t size() const noexcept {
			return m_size;
		}

		constexpr T *begin() const noexcept {
			return m_begin;
		}

		constexpr T *end() const noexcept {
			return m_begin + m_size;
		}

	private:
		constexpr initializer_list(T *begin, size_t size) noexcept: m_begin(begin), m_size(size) {}
};

}

#endif