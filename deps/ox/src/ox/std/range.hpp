/*
 * Copyright 2015 - 2022 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

namespace ox {

template<typename T>
class range {

	private:
		T m_begin = 0;
		T m_end = 0;

	public:
		constexpr range(T begin, T end) noexcept: m_begin(begin), m_end(end) {
		}
};

}
