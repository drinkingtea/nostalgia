/*
 * Copyright 2015 - 2022 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "stacktrace.hpp"
#include "trace.hpp"

#include "assert.hpp"

namespace ox {

void panic(const char *file, int line, const char *panicMsg, const Error &err) noexcept {
	oxErrf("\033[31;1;1mPANIC:\033[0m [{}:{}]: {}\n", file, line, panicMsg);
	if (err.msg) {
		oxErrf("\tError Message:\t{}\n", err.msg);
	}
	oxErrf("\tError Code:\t{}\n", static_cast<ErrorCode>(err));
	if (err.file != nullptr) {
		oxErrf("\tError Location:\t{}:{}\n", err.file, err.line);
	}
#ifdef OX_USE_STDLIB
	printStackTrace(2);
	oxTrace("panic").del("") << "Panic: " << panicMsg << " (" << file << ":" << line << ")";
	std::abort();
#else
	while (1);
#endif
}

}
