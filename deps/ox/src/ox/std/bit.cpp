/*
 * Copyright 2015 - 2022 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "bit.hpp"

static_assert(ox::onMask<int>(1) == 0b0001);
static_assert(ox::onMask<int>(2) == 0b0011);
static_assert(ox::onMask<int>(3) == 0b0111);
static_assert(ox::onMask<int>(4) == 0b1111);

static_assert(ox::MaxValue<int8_t>   ==                   127);
static_assert(ox::MaxValue<int16_t>  ==                 32767);
static_assert(ox::MaxValue<int32_t>  ==            2147483647);
static_assert(ox::MaxValue<int64_t>  ==   9223372036854775807);
static_assert(ox::MaxValue<uint8_t>  ==                   255);
static_assert(ox::MaxValue<uint16_t> ==                 65535);
static_assert(ox::MaxValue<uint32_t> ==            4294967295);
static_assert(ox::MaxValue<uint64_t> == 18446744073709551615u);
