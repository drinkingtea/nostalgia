/*
 * Copyright 2015 - 2022 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "typetraits.hpp"

namespace ox {

template<typename T>
[[nodiscard]]
constexpr T min(T a, T b) noexcept requires(ox::is_integral_v<T>) {
	return a < b ? a : b;
}

template<typename T>
[[nodiscard]]
constexpr const T &min(const T &a, const T &b) noexcept requires(!ox::is_integral_v<T>) {
	return a < b ? a : b;
}

template<typename T>
[[nodiscard]]
constexpr T max(T a, T b) noexcept requires(ox::is_integral_v<T>) {
	return a > b ? a : b;
}

template<typename T>
[[nodiscard]]
constexpr const T &max(const T &a, const T &b) noexcept requires(!ox::is_integral_v<T>) {
	return a > b ? a : b;
}

template<typename T>
[[nodiscard]]
constexpr T clamp(T v, T lo, T hi) noexcept requires(ox::is_integral_v<T>) {
	return min(ox::max(v, lo), hi);
}

template<typename T>
[[nodiscard]]
constexpr const T &clamp(const T &v, const T &lo, const T &hi) noexcept requires(!ox::is_integral_v<T>) {
	return min(ox::max(v, lo), hi);
}

template<typename I>
[[nodiscard]]
constexpr I pow(I v, int e) noexcept {
	I out = 1;
	for (I i = 0; i < e; i++) {
		out *= v;
	}
	return out;
}

}
