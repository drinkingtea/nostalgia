/*
 * Copyright 2015 - 2023 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

namespace ox {

[[nodiscard]]
constexpr bool all_of(auto begin, auto end, auto pred) noexcept {
	while (begin != end) {
		if (!pred(*begin)) {
			return false;
		}
		++begin;
	}
	return true;
}

[[nodiscard]]
constexpr bool any_of(auto begin, auto end, auto pred) noexcept {
	while (begin != end) {
		if (pred(*begin)) {
			return true;
		}
		++begin;
	}
	return false;
}

}
