/*
 * Copyright 2015 - 2023 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "basestringview.hpp"
#include "bstring.hpp"
#include "string.hpp"
#include "stringliteral.hpp"

namespace ox {

class CStringView: public detail::BaseStringView {
	public:
		constexpr CStringView() noexcept = default;

		constexpr CStringView(CStringView const&sv) noexcept = default;

		constexpr CStringView(StringLiteral const&str) noexcept: BaseStringView(str.data(), str.len()) {}

		template<std::size_t SmallStrSz>
		constexpr CStringView(BasicString<SmallStrSz> const&str) noexcept: BaseStringView(str.data(), str.len()) {}

		template<std::size_t SmallStrSz>
		constexpr CStringView(BString<SmallStrSz> const&str) noexcept: BaseStringView(str.data(), str.len()) {}

		constexpr CStringView(std::nullptr_t) noexcept {}

		constexpr CStringView(const char *str) noexcept: BaseStringView(str) {}

		constexpr CStringView(const char *str, std::size_t len) noexcept: BaseStringView(str, len) {}

		constexpr auto &operator=(CStringView const&other) noexcept {
			if (&other != this) {
				set(other.data(), other.len());
			}
			return *this;
		}

		[[nodiscard]]
		constexpr const char *c_str() const noexcept {
			return data();
		}

};

}

