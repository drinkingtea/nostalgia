/*
 * Copyright 2015 - 2023 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <ox/std/defines.hpp>

#include "vec.hpp"

namespace ox {

static_assert([] {
	Vec2 v(1, 2);
	return v.x == 1 && v.y == 2 && v[0] == 1 && v[1] == 2 && v.size() == 2;
}());

}
