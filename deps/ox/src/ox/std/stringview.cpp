/*
 * Copyright 2015 - 2022 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "stringview.hpp"

static_assert(ox::StringView("Read").bytes() == 4);
static_assert(ox::StringView("Read") == ox::StringView("Read"));
static_assert(ox::StringView("Read") != ox::StringView("Write"));
static_assert(ox::StringView("Write") != ox::StringView("Read"));
static_assert(ox::StringView("Write") != ox::StringView(""));
static_assert(ox::StringView("") != ox::StringView("Read"));
static_assert(ox::StringView("") == ox::StringView(""));
static_assert(ox::StringView("") == "");
static_assert("Read" == ox::StringView("Read"));
static_assert(ox::StringView("Read") == ox::StringView("Read"));
