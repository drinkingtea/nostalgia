/*
 * Copyright 2015 - 2022 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "string.hpp"

namespace ox {

template class BasicString<8>;

static_assert(StringView("Write") != String(""));
static_assert(String("Write") != StringView(""));
static_assert(String("Write") == StringView("Write"));
static_assert(String(StringView("Write")) == StringView("Write"));

}
