/*
 * Copyright 2015 - 2023 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include <ox/std/hashmap.hpp>
#include <ox/std/string.hpp>

namespace ox {

class ClArgs {
	private:
		HashMap<String, bool> m_bools;
		HashMap<String, String> m_strings;
		HashMap<String, int> m_ints;

	public:
		ClArgs(int argc, const char **args) noexcept;

		[[nodiscard]]
		bool getBool(ox::CRStringView arg, bool defaultValue) const noexcept;

		[[nodiscard]]
		String getString(ox::CRStringView argName, const char *defaultValue) const noexcept;

		[[nodiscard]]
		int getInt(ox::CRStringView arg, int defaultValue) const noexcept;

		[[nodiscard]]
		Result<bool> getBool(ox::CRStringView arg) const noexcept;

		[[nodiscard]]
		Result<String> getString(ox::CRStringView argName) const noexcept;

		Result<int> getInt(ox::CRStringView arg) const noexcept;

};

}
