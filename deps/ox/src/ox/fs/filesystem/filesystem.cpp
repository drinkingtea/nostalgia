/*
 * Copyright 2015 - 2023 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <ox/std/error.hpp>
#include <ox/std/utility.hpp>

#include "filesystem.hpp"

namespace ox {

Result<const char*> MemFS::directAccess(const FileAddress &addr) const noexcept {
	switch (addr.type()) {
		case FileAddressType::Inode:
			return directAccess(addr.getInode().value);
		case FileAddressType::ConstPath:
		case FileAddressType::Path:
			return directAccess(StringView(addr.getPath().value));
		default:
			return OxError(1);
	}
}

Error FileSystem::read(const FileAddress &addr, void *buffer, std::size_t size) noexcept {
	switch (addr.type()) {
		case FileAddressType::Inode:
			return readFileInode(addr.getInode().value, buffer, size);
		case FileAddressType::ConstPath:
		case FileAddressType::Path:
			return readFilePath(StringView(addr.getPath().value), buffer, size);
		default:
			return OxError(1);
	}
}

Result<Buffer> FileSystem::read(const FileAddress &addr) noexcept {
	oxRequire(s, stat(addr));
	Buffer buff(static_cast<std::size_t>(s.size));
	oxReturnError(read(addr, buff.data(), buff.size()));
	return buff;
}

Result<Buffer> FileSystem::read(CRStringView path) noexcept {
	oxRequire(s, statPath(path));
	Buffer buff(static_cast<std::size_t>(s.size));
	oxReturnError(readFilePath(path, buff.data(), buff.size()));
	return buff;
}

Error FileSystem::read(const FileAddress &addr, std::size_t readStart, std::size_t readSize, void *buffer, std::size_t *size) noexcept {
	switch (addr.type()) {
		case FileAddressType::Inode:
			return read(addr.getInode().value, readStart, readSize, buffer, size);
		case FileAddressType::ConstPath:
		case FileAddressType::Path:
			return OxError(2, "Unsupported for path lookups");
		default:
			return OxError(1);
	}
}

Error FileSystem::remove(const FileAddress &addr, bool recursive) noexcept {
	switch (addr.type()) {
		case FileAddressType::Inode:
			return remove(addr.getInode().value, recursive);
		case FileAddressType::ConstPath:
		case FileAddressType::Path:
			return remove(StringView(addr.getPath().value), recursive);
		default:
			return OxError(1);
	}
}

Error FileSystem::write(const FileAddress &addr, const void *buffer, uint64_t size, FileType fileType) noexcept {
	switch (addr.type()) {
		case FileAddressType::Inode:
			return writeFileInode(addr.getInode().value, buffer, size, fileType);
		case FileAddressType::ConstPath:
		case FileAddressType::Path:
			return writeFilePath(StringView(addr.getPath().value), buffer, size, fileType);
		default:
			return OxError(1);
	}
}

Result<FileStat> FileSystem::stat(const FileAddress &addr) const noexcept {
	switch (addr.type()) {
		case FileAddressType::Inode:
			return statInode(addr.getInode().value);
		case FileAddressType::ConstPath:
		case FileAddressType::Path:
			return statPath(StringView(addr.getPath().value));
		default:
			return OxError(1);
	}
}

template class FileSystemTemplate<FileStore16, Directory16>;
template class FileSystemTemplate<FileStore32, Directory32>;

}
