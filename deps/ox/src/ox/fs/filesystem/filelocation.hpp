/*
 * Copyright 2015 - 2022 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include <ox/std/std.hpp>
#include <ox/model/def.hpp>
#include <ox/model/typenamecatcher.hpp>
#include <ox/model/types.hpp>

namespace ox {

enum class FileAddressType: int8_t {
	None = -1,
	Path,
	ConstPath,
	Inode,
};

class FileAddress {

	template<typename T>
	friend constexpr Error model(T *h, CommonPtrWith<FileAddress> auto *fa) noexcept;

	public:
		static constexpr auto TypeName = "net.drinkingtea.ox.FileAddress";
		static constexpr auto TypeVersion = 1;

		union Data {
			static constexpr auto TypeName = "net.drinkingtea.ox.FileAddress.Data";
			static constexpr auto TypeVersion = 1;
			char *path = nullptr;
			const char *constPath;
			uint64_t inode;
		};

	protected:
		FileAddressType m_type = FileAddressType::None;
		Data m_data;

	public:
		constexpr FileAddress() noexcept {
			m_data.inode = 0;
			m_type = FileAddressType::None;
		}

		FileAddress(const FileAddress &other) noexcept;

		FileAddress(FileAddress &&other) noexcept;

		FileAddress(std::nullptr_t) noexcept;

		FileAddress(uint64_t inode) noexcept;

		explicit FileAddress(CRStringView path) noexcept;

		constexpr FileAddress(ox::StringLiteral path) noexcept;

		constexpr ~FileAddress() noexcept;

		FileAddress &operator=(const FileAddress &other) noexcept;

		FileAddress &operator=(FileAddress &&other) noexcept;

		bool operator==(CRStringView path) const noexcept;

		[[nodiscard]]
		constexpr FileAddressType type() const noexcept {
			switch (m_type) {
				case FileAddressType::Path:
				case FileAddressType::ConstPath:
					return FileAddressType::Path;
				default:
					return m_type;
			}
		}

		constexpr Result<uint64_t> getInode() const noexcept {
			switch (m_type) {
				case FileAddressType::Inode:
					return m_data.inode;
				default:
					return OxError(1);
			}
		}

		constexpr Result<ox::StringView> getPath() const noexcept {
			switch (m_type) {
				case FileAddressType::Path:
					return ox::StringView(m_data.path);
				case FileAddressType::ConstPath:
					return ox::StringView(m_data.constPath);
				default:
					return OxError(1);
			}
		}

		explicit constexpr operator bool() const noexcept {
			return m_type != FileAddressType::None;
		}

	private:
		/**
		 * Cleanup memory allocations.
		 */
		constexpr void cleanup() noexcept;

		/**
		 * Clears fields, but does not delete allocations.
		 */
		constexpr void clear() noexcept;

};

constexpr FileAddress::FileAddress(ox::StringLiteral path) noexcept {
	m_data.constPath = path.c_str();
	m_type = FileAddressType::ConstPath;
}

constexpr FileAddress::~FileAddress() noexcept {
	cleanup();
}

constexpr void FileAddress::cleanup() noexcept {
	if (m_type == FileAddressType::Path) {
		safeDeleteArray(m_data.path);
		clear();
	}
}

constexpr void FileAddress::clear() noexcept {
	m_data.path = nullptr;
	m_type = FileAddressType::None;
}

template<>
constexpr const char *getModelTypeName<FileAddress::Data>() noexcept {
	return FileAddress::Data::TypeName;
}

template<>
constexpr const char *getModelTypeName<FileAddress>() noexcept {
	return FileAddress::TypeName;
}

template<typename T>
constexpr Error model(T *h, CommonPtrWith<FileAddress::Data> auto *obj) noexcept {
	oxReturnError(h->template setTypeInfo<FileAddress::Data>());
	oxReturnError(h->fieldCString("path", &obj->path));
	oxReturnError(h->fieldCString("constPath", &obj->path));
	oxReturnError(h->field("inode", &obj->inode));
	return {};
}

template<typename T>
constexpr Error model(T *h, CommonPtrWith<FileAddress> auto *fa) noexcept {
	oxReturnError(h->template setTypeInfo<FileAddress>());
	if constexpr(T::opType() == OpType::Reflect) {
		int8_t type = 0;
		oxReturnError(h->field("type", &type));
		oxReturnError(h->field("data", UnionView(&fa->m_data, 0)));
	} else if constexpr(T::opType() == OpType::Read) {
		auto type = static_cast<int8_t>(fa->m_type);
		oxReturnError(h->field("type", &type));
		fa->m_type = static_cast<FileAddressType>(type);
		oxReturnError(h->field("data", UnionView(&fa->m_data, static_cast<int>(fa->m_type))));
	} else if constexpr(T::opType() == OpType::Write) {
		auto type = static_cast<int8_t>(fa->m_type);
		oxReturnError(h->field("type", &type));
		oxReturnError(h->field("data", UnionView(&fa->m_data, static_cast<int>(fa->m_type))));
	}
	return {};
}

}
