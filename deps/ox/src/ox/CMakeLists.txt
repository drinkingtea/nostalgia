if(${CMAKE_SYSTEM_NAME} STREQUAL "Windows")
	set(OX_OS_WINDOWS TRUE)
endif()
if(${CMAKE_SYSTEM_NAME} STREQUAL "FreeBSD")
	set(OX_OS_FREEBSD TRUE)
else()
	set(OX_OS_FREEBSD FALSE)
endif()

if(${CMAKE_SYSTEM_NAME} STREQUAL "Linux")
	set(OX_OS_LINUX TRUE)
else()
	set(OX_OS_LINUX FALSE)
endif()

if(OX_USE_STDLIB)
	add_subdirectory(oc)
endif()
add_subdirectory(clargs)
add_subdirectory(claw)
add_subdirectory(event)
add_subdirectory(fs)
add_subdirectory(logconn)
add_subdirectory(mc)
add_subdirectory(model)
add_subdirectory(preloader)
add_subdirectory(std)
