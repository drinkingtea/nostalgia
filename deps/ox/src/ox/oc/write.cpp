/*
 * Copyright 2015 - 2022 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "write.hpp"

namespace ox {

OrganicClawWriter::OrganicClawWriter(int unionIdx) noexcept: m_unionIdx(unionIdx) {
}

OrganicClawWriter::OrganicClawWriter(Json::Value json, int unionIdx) noexcept:
	m_json(std::move(json)),
	m_unionIdx(unionIdx) {
}

Error OrganicClawWriter::fieldCString(const char *key, const char *const*val, int len) noexcept {
	if (targetValid() && len) {
		value(key) = *val;
	}
	++m_fieldIt;
	return OxError(0);
}

Error OrganicClawWriter::fieldCString(const char *key, const char *const*val) noexcept {
	return fieldCString(key, const_cast<const char**>(val), {});
}

Error OrganicClawWriter::field(const char *key, const UUID *uuid) noexcept {
	const auto uuidStr = uuid->toString();
	if (targetValid() && uuidStr.len()) {
		value(key) = uuidStr.c_str();
	}
	++m_fieldIt;
	return {};
}

Json::Value &OrganicClawWriter::value(const char *key) noexcept {
	if (m_json.isArray()) {
		return m_json[m_fieldIt];
	} else {
		return m_json[key];
	}
}

}
