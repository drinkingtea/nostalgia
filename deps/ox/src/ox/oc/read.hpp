/*
 * Copyright 2015 - 2023 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include <json/json.h>

#include <ox/model/fieldcounter.hpp>
#include <ox/model/modelhandleradaptor.hpp>
#include <ox/model/optype.hpp>
#include <ox/model/typenamecatcher.hpp>
#include <ox/model/types.hpp>
#include <ox/std/buffer.hpp>
#include <ox/std/byteswap.hpp>
#include <ox/std/hashmap.hpp>
#include <ox/std/memops.hpp>
#include <ox/std/memory.hpp>
#include <ox/std/string.hpp>
#include <ox/std/uuid.hpp>

namespace ox {

class OrganicClawReader {

	private:
		Json::Value m_json;
		Json::ArrayIndex m_fieldIt = 0;
		int m_unionIdx = -1;

	public:
		OrganicClawReader() noexcept = default;

		OrganicClawReader(const uint8_t *buff, std::size_t buffSize);

		OrganicClawReader(const char *json, std::size_t buffSize);

		explicit OrganicClawReader(Json::Value json, int unionIdx = -1) noexcept;

		Error field(const char *key, int8_t *val) noexcept;
		Error field(const char *key, int16_t *val) noexcept;
		Error field(const char *key, int32_t *val) noexcept;
		Error field(const char *key, int64_t *val) noexcept;

		Error field(const char *key, uint8_t *val) noexcept;
		Error field(const char *key, uint16_t *val) noexcept;
		Error field(const char *key, uint32_t *val) noexcept;
		Error field(const char *key, uint64_t *val) noexcept;

		Error field(const char *key, bool *val) noexcept;

		// array handler
		template<typename T>
		Error field(const char *key, T *val, std::size_t len) noexcept;

		template<typename T>
		Error field(const char*, HashMap<String, T> *val) noexcept;

		template<typename T>
		Error field(const char *key, T *val) noexcept;

		template<typename U, bool force = false>
		Error field(const char *key, UnionView<U, force> val) noexcept;

		template<std::size_t L>
		Error field(const char *key, BasicString<L> *val) noexcept;

		template<std::size_t L>
		Error field(const char *key, BString<L> *val) noexcept;

		Error fieldCString(const char *key, char *val, std::size_t buffLen) noexcept;

		Error fieldCString(const char *key, char **val) noexcept;

		Error fieldCString(const char *key, char **val, std::size_t buffLen) noexcept;

		Error field(const char *key, UUID *val) noexcept;

		/**
		 * Reads an array length from the current location in the buffer.
		 * @param pass indicates that the parsing should iterate past the array length
		 */
		Result<std::size_t> arrayLength(const char *key, bool pass = true) noexcept;

		/**
		 * Reads an string length from the current location in the buffer.
		 */
		[[nodiscard]]
		std::size_t stringLength(const char *name) noexcept;

		template<typename T = void>
		constexpr ox::Error setTypeInfo() noexcept {
			return {};
		}

		template<typename T = void>
		constexpr ox::Error setTypeInfo(const char*) noexcept {
			return {};
		}

		template<typename T = void>
		constexpr ox::Error setTypeInfo(const char*, int, const Vector<String>& = {}) noexcept {
			return {};
		}

		template<typename T = void>
		constexpr ox::Error setTypeInfo(const char*, int, const Vector<String>& = {}, std::size_t = {}) noexcept {
			return {};
		}

		/**
		 * Returns a OrganicClawReader to parse a child object.
		 */
		[[nodiscard]]
		OrganicClawReader child(const char *key, int unionIdx = -1) noexcept;

		// compatibility stub
		constexpr void nextField() noexcept {}

		[[nodiscard]]
		bool fieldPresent(const char *key) noexcept;

		[[nodiscard]]
		int whichFieldPresent(const char *name, const ModelUnion &u) const noexcept;

		[[nodiscard]]
		static constexpr auto opType() noexcept {
			return OpType::Read;
		}

	private:
		[[nodiscard]]
		Json::Value &value(const char *key) noexcept;

		[[nodiscard]]
		bool targetValid() const noexcept;

};

template<typename T>
Error OrganicClawReader::field(const char *key, T *val) noexcept {
	auto err = OxError(0);
	if constexpr(isVector_v<T>) {
		const auto &srcVal = value(key);
		const auto srcSize = srcVal.size();
		val->resize(srcSize);
		err = field(key, val->data(), val->size());
	} else if constexpr(isArray_v<T>) {
		const auto &srcVal = value(key);
		const auto srcSize = srcVal.size();
		if (srcSize > val->size()) {
			err = OxError(1, "Input array is too long");
		} else {
			err = field(key, val->data(), val->size());
		}
	} else if (targetValid()) {
		const auto &jv = value(key);
		if (jv.empty() || jv.isObject()) {
			auto reader = child(key);
			ModelHandlerInterface handler(&reader);
			err = model(&handler, val);
		} else {
			err = OxError(1, "Type mismatch");
		}
	}
	++m_fieldIt;
	return err;
}

template<typename U, bool force>
Error OrganicClawReader::field(const char *key, UnionView<U, force> val) noexcept {
	auto err = OxError(0);
	if (targetValid()) {
		const auto &jv = value(key);
		if (jv.empty() || jv.isObject()) {
			auto reader = child(key, val.idx());
			ModelHandlerInterface handler(&reader);
			err = model(&handler, val.get());
		} else {
			err = OxError(1, "Type mismatch");
		}
	}
	++m_fieldIt;
	return err;
}

template<std::size_t L>
Error OrganicClawReader::field(const char *key, BasicString<L> *val) noexcept {
	auto err = OxError(0);
	if (targetValid()) {
		const auto &jv = value(key);
		if (jv.empty()) {
			*val = BasicString<L>{};
		} else if (jv.isString()) {
			*val = jv.asString().c_str();
		} else {
			err = OxError(1, "Type mismatch");
		}
	}
	++m_fieldIt;
	return err;
}

template<std::size_t L>
Error OrganicClawReader::field(const char *key, BString<L> *val) noexcept {
	return fieldCString(key, val->data(), val->cap());
}

// array handler
template<typename T>
Error OrganicClawReader::field(const char *key, T *val, std::size_t valLen) noexcept {
	const auto &srcVal = value(key);
	if (!srcVal.isNull() && !srcVal.isArray()) {
		return OxError(1, "Type mismatch");
	}
	auto srcSize = srcVal.size();
	if (srcSize > valLen) {
		return OxError(1);
	}
	OrganicClawReader r(srcVal);
	ModelHandlerInterface handler{&r};
	for (decltype(srcSize) i = 0; i < srcSize; ++i) {
		oxReturnError(handler.field("", &val[i]));
	}
	return OxError(0);
}

template<typename T>
Error OrganicClawReader::field(const char *key, HashMap<String, T> *val) noexcept {
	const auto &srcVal = value(key);
	if (!srcVal.isObject()) {
		return OxError(1, "Type mismatch");
	}
	auto keys = srcVal.getMemberNames();
	auto srcSize = srcVal.size();
	OrganicClawReader r(srcVal);
	ModelHandlerInterface handler{&r};
	for (decltype(srcSize) i = 0; i < srcSize; ++i) {
		const auto k = keys[i].c_str();
		oxReturnError(handler.field(k, &val->operator[](k)));
	}
	return OxError(0);
}

Error readOC(const char *buff, std::size_t buffSize, auto *val) noexcept {
	// OrganicClawReader constructor can throw, but readOC should return its errors.
	try {
		Json::Value doc;
		Json::CharReaderBuilder parserBuilder;
		auto parser = UniquePtr<Json::CharReader>(parserBuilder.newCharReader());
		if (!parser->parse(buff, buff + buffSize, &doc, nullptr)) {
			return OxError(1, "Could not parse JSON");
		}
		OrganicClawReader reader(buff, buffSize);
		ModelHandlerInterface handler(&reader);
		return model(&handler, val);
	} catch (const Error &err) {
		return err;
	} catch (...) {
		return OxError(1, "Unknown Error");
	}
}

template<typename T>
Result<T> readOC(const char *json, std::size_t jsonLen) noexcept {
	T val;
	oxReturnError(readOC(json, jsonLen, &val));
	return val;
}

template<typename T>
Result<T> readOC(const char *json) noexcept {
	return readOC<T>(json, ox_strlen(json));
}

template<typename T>
Result<T> readOC(const Buffer &buff) noexcept {
	return readOC<T>(buff.data(), buff.size());
}

}
