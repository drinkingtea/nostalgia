/*
 * Copyright 2015 - 2023 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <cstdio>

#include <ox/std/reader.hpp>
#include <ox/std/trace.hpp>

#include "claw.hpp"


int main(int argc, char **args) {
	if (argc > 2) {
		oxErr("Too many arguments");
		return -1;
	}
	auto const file = argc == 1 ? stdin : fopen(args[1], "r");
	if (fseek(file, 0, SEEK_END)) {
		oxErr("Could not get file size\n");
		return -2;
	}
	auto const size = static_cast<std::size_t>(ftell(file));
	oxDebugf("{}", size);
	return 0;
}
