/*
 * Copyright 2016 - 2022 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#include "preloader.hpp"

namespace ox {

template class Preloader<NativePlatSpec>;

}
