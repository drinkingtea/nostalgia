/*
 * Copyright 2015 - 2022 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "signal.hpp"

namespace ox {

#ifndef OX_OS_BareMetal
template class Signal<const SignalHandler*>;
#endif
template class Signal<Error(const SignalHandler*)>;

SignalHandler::~SignalHandler() noexcept {
	destruction.emit(this);
}

}

/*
 * 1. ProjectExplorer::fileOpened cannot notify StudioUI that it is being destroyed.
 * 2. StudioUI tries to unsubscribe from ProjectExplorer::fileOpened upon its destruction.
 * 3. Segfault
 */
