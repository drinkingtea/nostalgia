/*
 * Copyright 2015 - 2022 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#undef NDEBUG

#include <map>
#include <functional>
#include <ox/event/signal.hpp>
#include <ox/std/std.hpp>

struct TestStruct: public ox::SignalHandler {
	int value = 0;
	ox::Error method(int i) noexcept {
		value = i;
		return OxError(0);
	}
};

std::map<ox::StringView, std::function<ox::Error()>> tests = {
	{
		"test1",
		[] {
			ox::Signal<ox::Error(int)> signal;
			signal.connect([](int i) -> ox::Error {
				return OxError(i != 5);
			});
			TestStruct ts;
			signal.connect(&ts, &TestStruct::method);
			oxReturnError(signal.emitCheckError(5));
			oxReturnError(OxError(ts.value != 5));
			return OxError(0);
		}
	},
};

int main(int argc, const char **args) {
	if (argc < 2) {
		oxError("Must specify test to run");
	}
	auto const testName = args[1];
	auto const func = tests.find(testName);
	if (func != tests.end()) {
		oxAssert(func->second(), "Test returned Error");
		return 0;
	}
	return -1;
}
