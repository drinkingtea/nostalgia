/*
 * Copyright 2015 - 2023 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#if defined(DEBUG) && !defined(OX_BARE_METAL)
#define OX_INIT_DEBUG_LOGGER(loggerName, appName) \
	ox::LoggerConn loggerName; \
    { \
		const auto loggerErr = (loggerName).initConn(appName); \
		if (loggerErr) { \
			oxErrf("Could not connect to logger: {} ({}:{})\n", toStr(loggerErr), loggerErr.file, loggerErr.line); \
		} else { \
			ox::trace::setLogger(&(loggerName)); \
		} \
		ox::trace::init(); \
	}
#else
#define OX_INIT_DEBUG_LOGGER(loggerName, appName)
#endif


