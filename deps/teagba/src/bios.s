//
// Copyright 2016 - 2023 gary@drinkingtea.net
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

.section .iwram, "ax", %progbits
.thumb
.align

.global teagba_halt
.type teagba_halt, %function
teagba_halt:
	swi 2
	bx lr

.global teagba_stop
.type teagba_stop, %function
teagba_stop:
	swi 3
	bx lr

.global teagba_intrwait
.type teagba_intrwait, %function
teagba_intrwait:
	swi 4
	bx lr

.global teagba_vblankintrwait
.type teagba_vblankintrwait, %function
teagba_vblankintrwait:
	swi 5
	bx lr

// vim: ft=armv4
