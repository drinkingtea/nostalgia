/*
 * Copyright 2016 - 2023 gary@drinkingtea.net
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

// stubs for building TeaGBA for PC targets, for purposes of not having to
// switch back and forth between builds when editing GBA files

extern "C" {

void teagba_halt() {}

void teagba_stop() {}

void teagba_intrwait(unsigned, unsigned) {}

void teagba_vblankintrwait() {}

}
