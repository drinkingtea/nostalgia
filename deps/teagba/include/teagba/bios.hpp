/*
 * Copyright 2016 - 2023 Gary Talent (gary@drinkingtea.net). All rights reserved.
 */

#pragma once

// Functions for accessing BIOS calls

extern "C" {

// waits for any interrupt
void teagba_halt();

void teagba_stop();

// waits for interrupts specified in interSubs
void teagba_intrwait(unsigned discardExistingIntrs, unsigned intrSubs);

// waits for vblank interrupt
void teagba_vblankintrwait();

}

namespace teagba {

// waits for any interrupt
inline auto halt() noexcept {
	return teagba_halt();
}

inline auto stop() noexcept {
	return teagba_stop();
}

// waits for interrupts specified in interSubs
inline auto intrwait(unsigned discardExistingIntrs, unsigned intrSubs) noexcept {
	return teagba_intrwait(discardExistingIntrs, intrSubs);
}

// waits for vblank interrupt
inline auto vblankintrwait() noexcept {
	return teagba_vblankintrwait();
}

}